#!/bin/sh

day="$(printf '%02d' "$1")"
part="$2"
inputFile="res/input_$1.txt"

[ -x "prepare" ] || exit 0

if ! ./prepare "$day" "$part" "$inputFile"; then
  echo 'eh ge hebt precies niet alles geïnstalleerd staan'
else
  if [ -x "run" ]; then
    hyperfine "./run '$day' '$part' '$inputFile'"
  fi
fi

#if [ -x "clean" ]; then
#  ./clean
#fi

true
