# AoC2021

My submissions for the [Advent of Code](https://adventofcode.com) 2021.

Written in `Cpp` using some of the latest c++20 features like concepts.
This enables us to get functional like interfaces for our solvers.

## Creating a new daily entry

- Login to the website and download your session cookie.
- Save this cookie in your `.env` file in the `AoCCookie` variable.
- Run the `get_input` script.
  It downloads the input file of the specified day.

## Running

Run all solutions by just running the executable.

Run a specific day by passing the `day`, `part` and `filepath` as input arguments.

## Misc

I accidentally created a lexer+parser+interpreter+codegenerator on day 2. You can find it in `src/interpreter`. I'm planning on letting this grow with the challenges whenever an opcode exercise arrives.
