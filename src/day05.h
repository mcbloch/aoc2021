#ifndef AOC2021_CPP_DAY05_H
#define AOC2021_CPP_DAY05_H


#include <unordered_map>
#include <charconv>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Coord {
    int x;
    int y;

    Coord(int x, int y) : x(x), y(y) {}

    Coord() = default;

    static Coord parseCoord(std::string_view &coordString) {
        auto parts = FileUtils::splitString(coordString, ",");
        auto coord = Coord();
        (void) std::from_chars(parts[0].data(), parts[0].data() + parts[0].length(), coord.x);
        (void) std::from_chars(parts[1].data(), parts[1].data() + parts[1].length(), coord.y);
        return coord;
    }

    /**
     * Needed when there would be a hash collision
     */
    bool operator==(const Coord &other) const {
        return (x == other.x
                && y == other.y);
    }
};

namespace std {
    template<>
    struct hash<Coord> {
        std::size_t operator()(const Coord &k) const {
            using std::size_t;
            using std::hash;
            using std::string;

            return (1000 * k.x) + k.y;
        }
    };
}

struct Day05 {
    static const int idx = 5;

    static inline std::string countOverlappingPaths(const std::string &inputFilePath, bool diagonals = false) {
        auto vectors = FileUtils::readFileStrings(inputFilePath);
        std::unordered_map<Coord, int> mappie{};

        for (const auto &vectorLine : vectors) {
            auto coords = FileUtils::splitString(vectorLine, " -> ");
            auto cFrom = Coord::parseCoord(coords[0]);
            auto cTo = Coord::parseCoord(coords[1]);
            if ((cFrom.x == cTo.x || cFrom.y == cTo.y) || diagonals) {
                Coord c = Coord(cFrom.x, cFrom.y);
                while (c != cTo) {
                    (void) mappie.try_emplace(c, 0);
                    mappie[c]++;

                    if (cFrom.x < cTo.x) { c.x++; }
                    else if (cFrom.x > cTo.x) { c.x--; }
                    if (cFrom.y < cTo.y) { c.y++; }
                    else if (cFrom.y > cTo.y) { c.y--; }
                }
                (void) mappie.try_emplace(c, 0);
                mappie[c]++;
            }
        }
        int countMultiple = 0;
        for (auto[k, v] : mappie) {
            if (v > 1) { countMultiple++; }
        }

        return std::to_string(countMultiple);
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        return countOverlappingPaths(inputFilePath);
    }


    static inline std::string ex2(const std::string &inputFilePath) {
        return countOverlappingPaths(inputFilePath, true);
    }
};

static_assert(DaySolver<Day05>);


#endif //AOC2021_CPP_DAY05_H