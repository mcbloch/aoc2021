#ifndef AOC2021_CPP_INTERPRETER_H
#define AOC2021_CPP_INTERPRETER_H

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <string>
#include <vector>
#include <sstream>
#include <vector>
#include <utility>

#include "lexer.h"
#include "wordReaders.h"
#include "parser.h"
#include "visitor.h"

// TODO Use concepts to link the classes together?

// A PARSE TREE is a concrete representation of the source code.
// It preserves all the information of the source code, including trivial information such as separators, whitespaces, comments, etc.
// A token is a child in our parse tree

// A, AST is an abstract representation of the source code,
// and may not contain some of the information that is there in the source.

/**
 * Assumptions:
 * - All ops have an arity of 1
 */
class Interpreter : public Visitor {
public:
    std::string interpretFile(const std::string &filename) {
        auto r = FileWordReader(filename);
        Lexer lexer = Lexer(r);
        Parser parser(lexer);
        auto expr = parser.parse();
        expr->accept(this);

        return std::to_string(horizontal * depth);
    }

    void visit(const ExprNumber *expr) override {
        throw std::runtime_error("Not implemented: visit(ExprNumber*)");
    }

    void visit(const ExprOperator *expr) override {
        throw std::runtime_error("Not implemented: visit(ExprOperator*)");
    }

    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                horizontal += amount;
                depth += aim * amount;
                break;
            case TokenType::DOWN:
                aim += amount;
                break;
            case TokenType::UP:
                aim -= amount;
                break;
            default:
                break;
        }
    }

    void visit(const ExprExpression *expr) override {
        if (expr != nullptr) {
            visit(expr->function);
            visit(expr->right);
        }
    }

private:
    // Registers
    int horizontal = 0;
    int depth = 0;
    int aim = 0;

};

#endif //AOC2021_CPP_INTERPRETER_H
