#ifndef AOC2021_CPP_PARSER_H
#define AOC2021_CPP_PARSER_H

#include <ranges>
#include <utility>
#include <vector>

#include "lexer.h"
#include "visitor.h"

class Visitor;

// SyntaxTree
class Expr {
public:
    virtual ~Expr() = default;

    virtual void accept(Visitor *v) const = 0;
};

struct ExprOperator : Expr {
public:
    explicit ExprOperator(Token value) : value(std::move(value)) {}

    void accept(Visitor *v) const override { v->visit(this); }

    Token value;
};

struct ExprNumber : Expr {
public:
    ExprNumber(int number, Token value) : number(number), value(std::move(value)) {}

    void accept(Visitor *v) const override { v->visit(this); }

    int number;
    Token value;
};

struct ExprFunction : Expr {
public:
    ExprFunction(ExprOperator *op, ExprNumber *number) : op(op), number(number) {}

    void accept(Visitor *v) const override { v->visit(this); }

    ExprOperator *op;
    ExprNumber *number;
};

struct ExprExpression : Expr {
public:
    ExprExpression(ExprFunction *function, ExprExpression *right) : function(function), right(right) {}

    void accept(Visitor *v) const override { v->visit(this); }

    ExprFunction *function;
    ExprExpression *right;
};

template<word_reader R>
class Parser {
public:
    explicit Parser(Lexer<R> &lexer) : mLexer(lexer) {};

    Parser() = default;

    Parser(const Parser &) = delete;

    /**
     * The parser is responsible for reading the tokens from the lexer and producing the parse-tree.
     * It gets the next token from the lexer, analyzes it, and compare it against a defined grammar.
     * Then decides which of the grammar rule should be considered, and continue to parse according to the grammar.
     *
     * Sometimes reading multiple tokens is needed to decide the path or the grammar rule to be considered
     *
     * Each method produces a syntax tree for that rule
     *
     * TODO Handle errors.
     *   Needed: A way to generically output errors and stop graciously
     *   Nice to have: be able to continue parsing even after a crash
     */
    inline ExprExpression *parse() {
        return parseExpression();
    }

    inline ExprExpression *parseExpression() {
        if (mLexer.peek().type == TokenType::Eof) {
            return nullptr;
        }
        ExprFunction *exprFunc = parseFunction();
        ExprExpression *expr = parseExpression();
        return new ExprExpression{.function = exprFunc, .right = expr};
    }

    inline ExprFunction *parseFunction() {
        return new ExprFunction{.op = parseOperator(), .number = parseNumber()};
    }

    inline ExprOperator *parseOperator() {
        if (match({TokenType::DOWN, TokenType::UP, TokenType::FORWARD})) {
            return new ExprOperator{.value = mLexer.consume()};
        }
        throw std::runtime_error("Parser exception. Invalid operator");
    }

    inline ExprNumber *parseNumber() {
        Token word = mLexer.consume();
        return new ExprNumber{.number = std::stoi(word.value), .value = word};
    }

    // HELPERS
    inline bool match(std::vector<TokenType> types) {
        Token peekToken = mLexer.peek();
        if (peekToken.type == TokenType::Eof) { return false; }

        return std::any_of(types.begin(), types.end(), [&](auto type) {
            return peekToken.type == type;
        });
    }

private:
    Lexer<R> &mLexer;
};

#endif //AOC2021_CPP_PARSER_H
