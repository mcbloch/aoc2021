#ifndef AOC2021_CPP_CODEGENERATOR_H
#define AOC2021_CPP_CODEGENERATOR_H

#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <cstdio>
#include <cstdlib>

#include "lexer.h"
#include "parser.h"
#include "visitor.h"

class CodeGenerator : public Visitor {
public:
    CodeGenerator(std::string runCmd, std::string extension) :
            Visitor(),
            runCmd(std::move(runCmd)), extension(std::move(extension)) {
    }

    std::string generateAndRun(const std::string &filename) {
        std::string resultFile = "generatedCode." + extension;
        outFile.open(resultFile);
        if (!outFile.is_open()) { return "Could not open outfile"; }

        initFile();

        auto r = FileWordReader(filename);
        Lexer lexer = Lexer(r);
        Parser parser(lexer);
        auto expr = parser.parse();
        expr->accept(this);

        finishFile();

        outFile.close();

        char buffer[100];
        std::sprintf(buffer, runCmd.c_str(), resultFile.c_str());
        std::system(buffer);

        return "";
    }

protected:
    virtual void visit(const ExprFunction *expr) = 0;

    void visit(const ExprExpression *expr) override {
        if (expr != nullptr) {
            visit(expr->function);
            visit(expr->right);
        }
    }

    void visit(const ExprNumber *expr) override {
        throw std::runtime_error("Not implemented: visit(ExprNumber*)");
    }

    void visit(const ExprOperator *expr) override {
        throw std::runtime_error("Not implemented: visit(ExprOperator*)");
    }

protected:
    std::string runCmd;
    std::string extension;
    std::ofstream outFile;

    virtual void initFile() = 0;

    virtual void finishFile() = 0;
};

class PythonGenerator : public CodeGenerator {
public:
    PythonGenerator() : CodeGenerator("python %s", "py") {}

private:
    void initFile() override {
        outFile << "horizontal = 0" << std::endl;
        outFile << "depth = 0" << std::endl;
        outFile << "aim = 0" << std::endl;
    }

    void finishFile() override {
        outFile << "print(horizontal * depth)" << std::endl;
    }

public:
    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                outFile << "horizontal += " << amount << std::endl;
                outFile << "depth += aim * " << amount << std::endl;
                break;
            case TokenType::DOWN:
                outFile << "aim += " << amount << std::endl;
                break;
            case TokenType::UP:
                outFile << "aim -= " << amount << std::endl;
                break;
            default:
                break;
        }
    }
};

class RubyGenerator : public CodeGenerator {
public:
    RubyGenerator() : CodeGenerator("ruby %s", "rb") {}

private:
    void initFile() override {
        outFile << "horizontal = 0" << std::endl;
        outFile << "depth = 0" << std::endl;
        outFile << "aim = 0" << std::endl;
    }

    void finishFile() override {
        outFile << "puts(horizontal * depth)" << std::endl;
    }

    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                outFile << "horizontal += " << amount << std::endl;
                outFile << "depth += aim * " << amount << std::endl;
                break;
            case TokenType::DOWN:
                outFile << "aim += " << amount << std::endl;
                break;
            case TokenType::UP:
                outFile << "aim -= " << amount << std::endl;
                break;
            default:
                break;
        }
    }
};

class CppGenerator : public CodeGenerator {
public:
    CppGenerator() : CodeGenerator("g++ -s -o run-cpp %s && ./run-cpp", "cpp") {}

private:
    void initFile() override {
        outFile << "#include <iostream>" << std::endl;
        outFile << "int main(){" << std::endl;
        outFile << "  int horizontal = 0;" << std::endl;
        outFile << "  int depth = 0;" << std::endl;
        outFile << "  int aim = 0;" << std::endl;
    }

    void finishFile() override {
        outFile << "  std::cout << horizontal * depth << std::endl;" << std::endl;
        outFile << "}" << std::endl;
    }

    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                outFile << "  horizontal += " << amount << ";" << std::endl;
                outFile << "  depth += aim * " << amount << ";" << std::endl;
                break;
            case TokenType::DOWN:
                outFile << "  aim += " << amount << ";" << std::endl;
                break;
            case TokenType::UP:
                outFile << "  aim -= " << amount << ";" << std::endl;
                break;
            default:
                break;
        }
    }
};

class CGenerator : public CodeGenerator {
public:
    CGenerator() : CodeGenerator("g++ -s -o run-c %s && ./run-c", "c") {}

private:
    void initFile() override {
        outFile << "#include <stdio.h>" << std::endl;
        outFile << "int main(){" << std::endl;
        outFile << "  int horizontal = 0;" << std::endl;
        outFile << "  int depth = 0;" << std::endl;
        outFile << "  int aim = 0;" << std::endl;
    }

    void finishFile() override {
        outFile << R"(  printf("%d\n", horizontal * depth);)" << std::endl;
        outFile << "}" << std::endl;
    }

    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                outFile << "  horizontal += " << amount << ";" << std::endl;
                outFile << "  depth += aim * " << amount << ";" << std::endl;
                break;
            case TokenType::DOWN:
                outFile << "  aim += " << amount << ";" << std::endl;
                break;
            case TokenType::UP:
                outFile << "  aim -= " << amount << ";" << std::endl;
                break;
            default:
                break;
        }
    }
};

class ClojureGenerator : public CodeGenerator {
public:
    ClojureGenerator() : CodeGenerator("clojerl %s", "clj") {}

private:
    void initFile() override {
        outFile << "(def state (atom {}))\n"
                   "\n"
                   "(defn get-state [key]\n"
                   "  (@state key))\n"
                   "\n"
                   "(defn update-state [key val]\n"
                   "  (swap! state assoc key val))" << std::endl;
        outFile << "(update-state \"horizontal\" 0)" << std::endl;
        outFile << "(update-state \"depth\" 0)" << std::endl;
        outFile << "(update-state \"aim\" 0)" << std::endl;
    }

    void finishFile() override {
        outFile << R"((println (* (get-state "horizontal") (get-state "depth"))))" << std::endl;
    }

    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                outFile << R"((update-state "horizontal" (+ (get-state "horizontal") )" << amount << "))" << std::endl;
                outFile << R"((update-state "depth" (+ (get-state "depth") (* (get-state "aim") )" << amount << ")))"
                        << std::endl;
                break;
            case TokenType::DOWN:
                outFile << R"((update-state "aim" (+ (get-state "aim") )" << amount << "))" << std::endl;
                break;
            case TokenType::UP:
                outFile << R"((update-state "aim" (- (get-state "aim") )" << amount << "))" << std::endl;
                break;
            default:
                break;
        }
    }
};

class GoGenerator : public CodeGenerator {
public:
    GoGenerator() : CodeGenerator("go run %s", "go") {}

private:

    void initFile() override {
        outFile << "package main\nimport(\"fmt\")\nfunc main(){" << std::endl;
        outFile << "  horizontal := 0" << std::endl;
        outFile << "  depth := 0" << std::endl;
        outFile << "  aim := 0" << std::endl;
    }

    void finishFile() override {
        outFile << "  fmt.Println(horizontal * depth)" << std::endl;
        outFile << "}" << std::endl;
    }

    void visit(const ExprFunction *expr) override {
        int amount = expr->number->number;

        switch (expr->op->value.type) {
            case TokenType::FORWARD:
                outFile << "  horizontal += " << amount << std::endl;
                outFile << "  depth += aim * " << amount << std::endl;
                break;
            case TokenType::DOWN:
                outFile << "  aim += " << amount << std::endl;
                break;
            case TokenType::UP:
                outFile << "  aim -= " << amount << std::endl;
                break;
            default:
                break;
        }
    }
};


#endif //AOC2021_CPP_CODEGENERATOR_H
