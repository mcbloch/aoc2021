#ifndef AOC2021_CPP_WORDREADERS_H
#define AOC2021_CPP_WORDREADERS_H

#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <vector>

// Normally a character reader is used but we'll just use words
template<typename T>
concept word_reader = requires(T &t){
    { t.peek() } ->std::convertible_to<std::string>;
    { t.consume() }->std::convertible_to<std::string>;
    { t.isEof() }->std::convertible_to<bool>;
};

class FileWordReader {
public:
    explicit FileWordReader(const std::string &mSourceFile) : mSourceFile(mSourceFile) {
        mSourceStream = std::ifstream(mSourceFile);
        if (!mSourceStream.is_open()) {
            std::cout << "Could not find or open source file." << std::endl;
        }
    }

    FileWordReader() = default;

    FileWordReader(const FileWordReader &) = delete;

    FileWordReader &operator=(const FileWordReader &) = delete;

    ~FileWordReader() {
        mSourceStream.close();
    }

    inline std::string peek() {
        std::string word;
        std::streampos sp = mSourceStream.tellg();
        mSourceStream >> word;
        mSourceStream.seekg(sp);
        return word;
    }

    inline std::string consume() {
        std::string word;
        // the normal input operator >> separates on whitespace and so can be used to read "words"
        mSourceStream >> word;
        return word;
    }

    inline bool isEof() {
        return !mSourceStream.good();
    }

private:
    std::string mSourceFile;
    std::ifstream mSourceStream;
};

static_assert(word_reader<FileWordReader>);


#endif //AOC2021_CPP_WORDREADERS_H
