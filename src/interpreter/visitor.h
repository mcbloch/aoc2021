#ifndef AOC2021_CPP_VISITOR_H
#define AOC2021_CPP_VISITOR_H

struct ExprExpression;
struct ExprFunction;
struct ExprOperator;
struct ExprNumber;

class Visitor {
public:
    virtual void visit(const ExprExpression *expr) = 0;

    virtual void visit(const ExprFunction *expr) = 0;

    virtual void visit(const ExprOperator *expr) = 0;

    virtual void visit(const ExprNumber *expr) = 0;
};

#endif //AOC2021_CPP_VISITOR_H
