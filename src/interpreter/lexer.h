#ifndef AOC2021_CPP_LEXER_H
#define AOC2021_CPP_LEXER_H

#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <sstream>
#include <vector>
#include <array>
#include <stdexcept>

#include "../utils/utils.h"

#include "wordReaders.h"

enum class TokenType {
    // Keyword, Predefined set known by lexer and parser
    FORWARD, DOWN, UP,
    Identifier, // Dynamic names
    Literal, // Dynamic values: String literals, numeric literals, ...
    SyntaxToken, // Predefined set of tokens such as arithmetic, separators, ...
    Eof,
};

struct Token {
    std::string value;
    TokenType type;
};

template<word_reader R>
class Lexer {
public:
    explicit Lexer(R &reader) : mReader(reader) {}

    Lexer() = default;

    ~Lexer() = default;

    Lexer(const Lexer &) = delete;

    Lexer &operator=(const Lexer &) = delete;

    /**
     * Evaluation of content.
     * Emit the relevant token
     * @return
     */
    Token processNumericLiteral(std::string word) {
        return Token{std::move(word), TokenType::Literal};
    }

    Token processStringLiteral(std::string word) {
        return Token{std::move(word), TokenType::Literal};
    }

    Token processKeyword(std::string word) {
        if (word == "forward") {
            return {word, TokenType::FORWARD};
        } else if (word == "down") {
            return {word, TokenType::DOWN};
        } else if (word == "up") {
            return {word, TokenType::UP};
        } else {
            wordError(word);
            return {};
        }
    }

    Token nextToken(std::string word) {
        // Scanning
        //   This could be a FSM. A certain start character changes the state to reading a string-literal, number, ...
        //   In this state a certain finishing character makes us generate the token and return to default state
        //   Everything in our parser is delimited by spaces so we do it more simple.

        // Scan input and determine the grammar rule
        if (word[0] >= '0' and word[0] <= '9') {
            return processNumericLiteral(word);
        } else if ((word[0] >= 'a' && word[0] <= 'z') ||
                   (word[0] >= 'A' && word[0] <= 'Z')) {
            return processKeyword(word);
        } else if (word.empty()) {
            mReader.consume();
            return consume();
        } else {
            wordError(word);
            return {};
        }
    }

    Token peek() {
        if (mReader.isEof()) {
            return {.value{}, .type =  TokenType::Eof};
        }
        std::string word = mReader.peek();
        return nextToken(word);
    }

    Token consume() {
        if (mReader.isEof()) {
            return Token{.value{}, .type =  TokenType::Eof};
        }
        std::string word = mReader.consume();
        return nextToken(word);
    }

    void wordError(const std::string &word) {
        throw std::runtime_error(
                std::string().append(
                        "Unknown word scanned in reader. Cannot find matching grammar rule.\n\t").append(
                        "word read: '").append(
                        toHex(word)).append(
                        "'"));
    }


    // Would be used when using character scanning instead of words
    static constexpr std::array<char, 10> numbers{
            '0', '1', '2', '3',
            '4', '5', '6', '7',
            '8', '9'};


private:
    R &mReader;
};

#endif //AOC2021_CPP_LEXER_H