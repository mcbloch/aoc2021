

- input as a stream of stuff
- can be an
    - op (operation) : like adition, substraction
        - has an arity = the amount of operands it takes
    - operands/args : like a number, string, ...


Based on
- https://medium.com/swlh/writing-a-parser-getting-started-44ba70bb6cc9
- https://medium.com/swlh/writing-a-parser-algorithms-and-implementation-a7c40f46493d

- https://craftinginterpreters.com/evaluating-expressions.html


- https://www.cs.man.ac.uk/~pjj/farrell/comp5.html
- https://www.toptal.com/scala/writing-an-interpreter
