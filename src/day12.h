#ifndef AOC2021_CPP_DAY12_H
#define AOC2021_CPP_DAY12_H

#include <string>
#include <vector>
#include <unordered_map>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Day12 {
    static const int idx = 12;

//    struct Node {
//        int index;
//        std::string name;
//    };
    typedef std::string Node;
    struct Edge {
        Node fromNode;
        Node toNode;
    };

    typedef std::vector<Node> Nodes;
    typedef std::vector<Edge> Edges;
    typedef std::vector<Node> Path;

    struct Graph {
        Edges edges{};
        bool hasDoubleSmall = false;
        std::unordered_map<std::string, std::vector<std::string>> neighbors;
    };

    static void followEdge(Graph &g, int *pathCount, Path &path, const Node &node,
                           bool(*validPath)(Graph &, const Path &, Node)) {

        // Check if we finish
        if (node == "end") {
            (*pathCount)++;
            return;
        }
        // Check we are not in a loop and if our advance is valid
        bool valid = validPath(g, path, node);
        if (!valid) { return; }

        // Add the current node to the path
        // Recursively add other nodes to the path
        (void) path.emplace_back(node);
        for (const auto &neigh: g.neighbors[node]) {
            followEdge(g, pathCount, path, neigh, validPath);
        }

        long c = std::count(path.begin(), path.end(), node);
        if (c == 2 && islower(node[0UL]) != 0) {
            g.hasDoubleSmall = false;
        }
        path.pop_back();
    }

    static inline std::string
    pathFinder(const std::string &inputFilePath, bool(*validPath)(Graph &, const Path &, Node)) {
        auto lines = FileUtils::readFileStrings(inputFilePath);
        // Construct graph
        Graph g{};
        for (const auto &line: lines) {
            auto parts = FileUtils::splitString(line, '-');
            g.edges.push_back({Node{parts[0]}, Node{parts[1]}});
        }
        // Cache the neighbours
        for (const auto &edge: g.edges) {
            if (g.neighbors.contains(edge.fromNode)) {
                g.neighbors[edge.fromNode].push_back(edge.toNode);
            } else {
                g.neighbors[edge.fromNode] = Nodes{edge.toNode};
            }
            if (g.neighbors.contains(edge.toNode)) {
                g.neighbors[edge.toNode].push_back(edge.fromNode);
            } else {
                g.neighbors[edge.toNode] = Nodes{edge.fromNode};
            }
        }

        // DFS
        Path currentPath{};
        int pathCount = 0;
        followEdge(g, &pathCount, currentPath, "start", validPath);


        return std::to_string(pathCount);
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        return pathFinder(inputFilePath, [](Graph &g, const Path &path, Node node) {
            if (islower(node[0UL]) != 0) {
                long c = std::count(path.begin(), path.end(), node);
                if (c >= 1) {
                    return false;
                }
            }
            return true;
        });
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        return pathFinder(inputFilePath, [](Graph &g, const Path &path, Node node) {
            long c = std::count(path.begin(), path.end(), node);
            if (node == "start" && c == 1) {
                return false;
            }
            if (islower(node[0UL]) != 0) {
                if (c >= 2) {
                    return false;
                } else if (c == 1) {
                    if (g.hasDoubleSmall) {
                        return false;
                    } else {
                        g.hasDoubleSmall = true;
                    }
                } else {}
            }

            return true;
        });
    }
};

static_assert(DaySolver<Day12>);

#endif //AOC2021_CPP_DAY12_H
