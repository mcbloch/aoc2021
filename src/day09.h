#ifndef AOC2021_CPP_DAY09_H
#define AOC2021_CPP_DAY09_H

#include <string>
#include <utility>
#include <vector>
#include <deque>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/bitvector.h"

struct Day09 {
    static const int idx = 9;

    typedef std::vector<std::vector<int>> grid;
    typedef std::tuple<int, int> coordinate;
    typedef std::vector<coordinate> coordinates;

    static grid readGrid(const std::string &inputFilePath) {
        std::vector<std::string> linesStr = FileUtils::readFileStrings(inputFilePath);
        std::vector<std::vector<int>> lines{};
        for (const auto &line : linesStr) {
            std::vector<int> ints;
            for (char c : line) {
                ints.emplace_back(c - '0');
            }
            lines.emplace_back(ints);
        }
        return lines;
    }

    static bool isLowPoint(const grid &grid, uint r, uint c) {
        return (
                (r == 0 || grid[r][c] < grid[r - 1][c]) &&
                (c == 0 || grid[r][c] < grid[r][c - 1]) &&
                (r == grid.size() - 1 || grid[r][c] < grid[r + 1][c]) &&
                (c == grid[0].size() - 1 || grid[r][c] < grid[r][c + 1])
        );
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        auto grid = readGrid(inputFilePath);

        int sum = 0;
        for (size_t r = 0; r < grid.size(); ++r) {
            for (size_t c = 0; c < grid[0].size(); ++c) {
                if (isLowPoint(grid, r, c)) {
                    sum += grid[r][c] + 1;
                }
            }
        }

        return std::to_string(sum);
    }

    static coordinates findLowPoints(const grid &grid) {
        coordinates lowPoints{};
        for (uint r = 0; r < grid.size(); ++r) {
            for (uint c = 0; c < grid[0].size(); ++c) {
                if (isLowPoint(grid, r, c)) {
                    lowPoints.emplace_back(coordinate{r, c});
                }
            }
        }
        return lowPoints;
    }

    static int findBasinSize(const grid &grid, coordinate lowPoint) {
        int count = 0;
        std::vector<BitVector<2>> markedPoints(grid.size());

        // Points of which we want to check their surroundings. A checked point is added to the fixedPoints
        std::deque<coordinate> pointsToCheck{lowPoint};
        // Add point higher until we are at nine. Probably not right. A point can have a steeper slope on the other side
        while (!pointsToCheck.empty()) {
            auto point = pointsToCheck.front();
            auto[r, c]=point;

            bool isNewPoint = !markedPoints[r].getBit(c);

            if (isNewPoint) {
                markedPoints[r].setBit(c);
                count++;

                // Up
                if (r != 0 && grid[r][c] < grid[r - 1][c] && grid[r - 1][c] != 9) {
                    pointsToCheck.emplace_back(r - 1, c);
                }
                // Left
                if (c != 0 && grid[r][c] < grid[r][c - 1] && grid[r][c - 1] != 9) {
                    pointsToCheck.emplace_back(r, c - 1);
                }
                // Down
                if (r != (int) grid.size() - 1 && grid[r][c] < grid[r + 1][c] && grid[r + 1][c] != 9) {
                    pointsToCheck.emplace_back(r + 1, c);
                }
                // Right
                if (c != (int) grid[0].size() - 1 && grid[r][c] < grid[r][c + 1] && grid[r][c + 1] != 9) {
                    pointsToCheck.emplace_back(r, c + 1);
                }

            }
            pointsToCheck.pop_front();
        }
        return count;
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        auto grid = readGrid(inputFilePath);
        auto lowPoints = findLowPoints(grid);
        std::vector<long> basinSizeForLowPoint{};
        for (auto lowPoint : lowPoints) {
            long basinSize = findBasinSize(grid, lowPoint);
            basinSizeForLowPoint.emplace_back(basinSize);
        }
        std::ranges::sort(basinSizeForLowPoint);
        long sum =
                basinSizeForLowPoint[basinSizeForLowPoint.size() - 3] *
                basinSizeForLowPoint[basinSizeForLowPoint.size() - 2] *
                basinSizeForLowPoint[basinSizeForLowPoint.size() - 1];

        return std::to_string(sum);
    }
};

static_assert(DaySolver<Day09>);

#endif //AOC2021_CPP_DAY09_H