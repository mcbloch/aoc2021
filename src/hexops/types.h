#ifndef AOC2021_CPP_HEXOPS_TYPES_H
#define AOC2021_CPP_HEXOPS_TYPES_H

#include <list>
#include <string>
#include <sstream>
#include <memory>
#include <climits>
#include <bitset>

struct Packet {
    std::bitset<3> packetVersion;
    std::bitset<3> typeID;

    // Type 4 | literal
    ulong literalValue;

    std::list<std::shared_ptr<Packet>> subPackets;

    virtual ulong parseContent(const std::string &source, ulong i) = 0;
    virtual ulong evaluate() = 0;

    inline ulong countVersionSums() const {
        ulong versionSum = packetVersion.to_ulong();
        for (const auto &p: subPackets) {
            versionSum += p->countVersionSums();
        }
        return versionSum;
    }
};

struct Literal : Packet {
    using Packet::Packet;

    virtual ulong parseContent(const std::string &source, ulong parseI) override;

    virtual ulong evaluate() override {
        return literalValue;
    }

};

struct Operator : public Packet {
    using Packet::Packet;

    virtual ulong parseContent(const std::string &source, ulong parseI) override;
};

struct Duo : public Operator {
    using Operator::Operator;

    ulong compareDuo(bool(*op)(ulong, ulong)) {
        ulong v1 = (*subPackets.begin())->evaluate();
        ulong v2 = (*std::next(subPackets.begin()))->evaluate();
        return op(v1, v2) ? 1UL : 0UL;
    }
};

struct Sum : public Operator {
    using Operator::Operator;

    virtual ulong evaluate() override {
        ulong res = 0UL;
        for (const auto &subE: subPackets) {
            res += subE->evaluate();
        }
        return res;
    }

};

struct Product : Operator {
    using Operator::Operator;

    virtual ulong evaluate() override {
        ulong res = 1UL;
        for (const auto &subE: subPackets) {
            res *= subE->evaluate();
        }
        return res;
    }

};

struct Min : Operator {
    using Operator::Operator;

    virtual ulong evaluate() override {
        ulong res = ULONG_MAX;
        for (const auto &subP: subPackets) {
            ulong val = subP->evaluate();
            if (val < res) { res = val; }
        }
        return res;
    }
};

struct Max : Operator {
    using Operator::Operator;

    virtual ulong evaluate() override {
        ulong res = 0UL;
        for (const auto &subP: subPackets) {
            ulong val = subP->evaluate();
            if (val > res) { res = val; }
        }
        return res;
    }
};

struct Larger : Duo {
    using Duo::Duo;

    virtual ulong evaluate() override {
        return compareDuo([](ulong v1, ulong v2) { return v1 > v2; });
    }
};

struct Smaller : Duo {
    using Duo::Duo;

    virtual ulong evaluate() override {
        return compareDuo([](ulong v1, ulong v2) { return v1 < v2; });
    }
};

struct Equal : Duo {
    using Duo::Duo;

    virtual ulong evaluate() override {
        return compareDuo([](ulong v1, ulong v2) { return v1 == v2; });
    }
};

#endif //AOC2021_CPP_HEXOPS_TYPES_H
