#ifndef AOC2021_CPP_HEXOPS_PARSER_H
#define AOC2021_CPP_HEXOPS_PARSER_H

#include <bitset>
#include <string>
#include <memory>
#include <sstream>
#include <iostream>

#include "types.h"
#include "../utils/strUtils.h"

static inline std::tuple<std::shared_ptr<Packet>, ulong> parsePacket(const std::string &strBin, ulong startI = 0UL) {
    ulong i = startI;
    std::bitset<3U> packetVersionBitset{strBin.substr(i, 3U)};
    std::bitset<3U> typeIDBitset{strBin.substr(i + 3U, 3U)};
    i += 6U;

    std::shared_ptr<Packet> packet;
    switch (typeIDBitset.to_ulong()) {
        case 0UL: {
            packet = std::make_shared<Sum>();
            break;
        }
        case 1UL: {
            packet = std::make_shared<Product>();
            break;
        }
        case 2UL: {
            packet = std::make_shared<Min>();
            break;
        }
        case 3UL: {
            packet = std::make_shared<Max>();
            break;
        }
        case 4UL: {
            packet = std::make_shared<Literal>();
            break;
        }
        case 5UL: {
            packet = std::make_shared<Larger>();
            break;
        }
        case 6UL: {
            packet = std::make_shared<Smaller>();
            break;
        }
        case 7UL: {
            packet = std::make_shared<Equal>();
            break;
        }
        default: {
            std::cout << "Unknown typeID!" << std::endl;
            break;
        }
    }
    packet->packetVersion = packetVersionBitset;
    packet->typeID = typeIDBitset;

    auto endIdx = packet->parseContent(strBin, i);

    return std::make_tuple<>(packet, endIdx);
}


// ------------------
//    -- PARSER --
// ------------------


ulong Literal::parseContent(const std::string &source, ulong parseI) {
    bool hasNext = true;
    std::stringstream literalBitstream;
    while (hasNext) {
        // Literal | Type 4
        std::bitset<1U> literalEndIndicator{source.substr(parseI, 1U)};
        std::bitset<4U> literalBitset{source.substr(parseI + 1U, 4U)};
        literalBitstream << literalBitset.to_string();
        if (literalEndIndicator.to_ulong() == 0UL) {
            hasNext = false;
        }
        parseI += 5U;
    }
    literalValue = bin_str_to_int(literalBitstream.str());
    return parseI;
}

ulong Operator::parseContent(const std::string &source, ulong i) {
    std::bitset<1U> lengthTypeIDBitset{source.substr(i, 1U)};
    i++;
    switch (lengthTypeIDBitset.to_ulong()) {
        case 0U: {
            std::bitset<15U> totalSubPacketBitLength{source.substr(i, 15U)};
            i += 15U;
            ulong maxI = i + totalSubPacketBitLength.to_ulong();
            while (i < maxI) {
                auto[nextPacket, endIdx] = parsePacket(source, i);
                i = endIdx;
                subPackets.push_back(nextPacket);
            }
            break;
        }
        case 1U: {
            std::bitset<11U> totalSubPacketCount{source.substr(i, 11U)};
            i += 11U;
            for (ulong subIdx = 0UL; subIdx < totalSubPacketCount.to_ulong(); subIdx++) {
                auto[nextPacket, endIdx] = parsePacket(source, i);
                i = endIdx;
                subPackets.push_back(nextPacket);
            }
            break;
        }
        default:
            break;
    }
    return i;
}


#endif //AOC2021_CPP_HEXOPS_PARSER_H
