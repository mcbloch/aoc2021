//
// Created by maxime on 12/16/21.
//

#ifndef AOC2021_CPP_HEXOPS_READER_H
#define AOC2021_CPP_HEXOPS_READER_H

#include <sstream>

#include "../utils/fileUtils.h"
#include "../utils/strUtils.h"

static inline std::string reader(const std::string &inputFilePath) {
    std::string strHex = FileUtils::readFile(inputFilePath);
    std::string strBin;

    std::stringstream binStream;

    for (const char &n: strHex) {
        binStream << hex_char_to_bin(n);
    }
    return binStream.str();
}


#endif //AOC2021_CPP_HEXOPS_READER_H
