#ifndef AOC2021_CPP_DAY14_H
#define AOC2021_CPP_DAY14_H

#include <string>
#include <vector>
#include <bits/stdc++.h>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/destructure.h"

struct Day14 {
    static const int idx = 14;

    typedef std::array<long, 26> charcount;

    static void addToFrequency(charcount &M, const char &c) {
        M[c - 'A']++;
    }


    static void
    countNums(charcount &M, char first, char second, long currentIterationDepth, long maxIterationDepth,
              const std::vector<std::string> &pairInsertionRulesMatch,
              const std::vector<char> &pairInsertionRulesInsert) {

        if (currentIterationDepth >= maxIterationDepth) {
            addToFrequency(M, first);
//            std::cout << polymerTemplate[0];
            std::cout << "Cut at D: " << currentIterationDepth << std::endl;
        } else {
            auto findIt = std::find_if(pairInsertionRulesMatch.begin(), pairInsertionRulesMatch.end(),
                                       [&](std::string s) {
                                           return s[0] == first && s[1] == second;
                                       });
            if (findIt != pairInsertionRulesMatch.end()) {
                uint findIdx = findIt - pairInsertionRulesMatch.begin();
                char middleChar = pairInsertionRulesInsert[findIdx];

                countNums(M, first, middleChar, currentIterationDepth + 1,
                          maxIterationDepth,
                          pairInsertionRulesMatch, pairInsertionRulesInsert);
                countNums(M, middleChar, second, currentIterationDepth + 1,
                          maxIterationDepth,
                          pairInsertionRulesMatch, pairInsertionRulesInsert);
            } else {
//                std::cout << polymerTemplate[0];
                std::cout << "Cut at D: " << currentIterationDepth << std::endl;
            }
        }
    }

    static uint getIndex(char first, char second) {
        return ((first - 'A') * 26U) + (second - 'A');
    }

    static long solve(const std::string &inputFilePath, uint stepIterations = 10U) {
        std::string line;
        std::ifstream inputFile(inputFilePath);

        (void) getline(inputFile, line);
        std::string polymerTemplate = line;

        (void) getline(inputFile, line);

        std::vector<std::string> pairInsertionRulesMatch{};
        std::vector<char> pairInsertionRulesInsert{};

        if (inputFile.is_open()) {
            while (getline(inputFile, line)) {
                auto parts = FileUtils::splitString(line, " -> ");
                auto[left, right] = destructure<2, std::vector<std::string_view>>(parts);
                (void) pairInsertionRulesMatch.emplace_back(left);
                (void) pairInsertionRulesInsert.emplace_back(right[0U]);
            }
            inputFile.close();
        } else {
            std::cout << RED << "Could not find or open input file: " << inputFilePath << RESET << std::endl;
        }

        // Pointer map per combo to 2 indexes (26*26 = 676)
        std::vector<std::tuple<uint, uint>> pointerMap(676U);
        std::vector<bool> pointerMapSet(676U, false);

        // Counter per char combo per iteration
        std::vector<std::vector<charcount>> charComboCounter(
                stepIterations, std::vector<charcount>(676U));

        // Create the pointer map
        for (uint i = 0U; i < pairInsertionRulesMatch.size(); i++) {
            uint comboIdx = getIndex(pairInsertionRulesMatch[i][0U], pairInsertionRulesMatch[i][1U]);
            uint backPtr = getIndex(pairInsertionRulesMatch[i][0U], pairInsertionRulesInsert[i]);
            uint frontPtr = getIndex(pairInsertionRulesInsert[i], pairInsertionRulesMatch[i][1U]);
            pointerMap[comboIdx] = {backPtr, frontPtr};
            pointerMapSet[comboIdx] = true;
        }

        // Seed the bottom layer with ones
        for (uint comboIdx = 0U; comboIdx < pointerMap.size(); comboIdx++) {
            charComboCounter[stepIterations - 1U][comboIdx][int(comboIdx % 26U)] = 1U;
        }
        // Now go from bottom to top of the iterations and work out the counters.
        for (int i = int(stepIterations) - 2; i >= 0; i--) {
            for (uint comboIdx = 0U; comboIdx < pointerMap.size(); comboIdx++) {
                auto&[backptr, frontptr] = pointerMap[comboIdx];
                for (uint cCountIdx = 0U; cCountIdx < 26U; cCountIdx++) {
                    charComboCounter[i][comboIdx][cCountIdx] =
                            charComboCounter[i + 1][backptr][cCountIdx] +
                            charComboCounter[i + 1][frontptr][cCountIdx] +
                            1;
                }
            }
        }

        charcount count{0};
        for (int i = 0; i < polymerTemplate.size() - 2; i++) {
            const int idx = getIndex(polymerTemplate[i], polymerTemplate[i + 1]);
            for (int cIdx = 0; cIdx < 26; cIdx++) {
                count[cIdx] += charComboCounter[0][idx][cIdx];
            }
        }
        count[polymerTemplate[polymerTemplate.size() - 1] - 'A']++;

        long highest = 0;
        long lowest = LONG_MAX;
        for (auto freq: count) {
            if (freq != 0) {
                if (freq > highest) {
                    highest = freq;
                } else if (freq < lowest) {
                    lowest = freq;
                } else {}
            }
        }


//
//        // Define an map
//        charcount M{0};
//
//        for (uint i = 0U; i < polymerTemplate.size(); i++) {
//            countNums(M, polymerTemplate[i], polymerTemplate[i + 1], 0, stepIterations,
//                      pairInsertionRulesMatch,
//                      pairInsertionRulesInsert);
//        }
//        addToFrequency(M, polymerTemplate[polymerTemplate.size() - 1]);

//        long highest = 0;
//        long lowest = LONG_MAX;
//        for (auto freq: M) {
//            if (freq != 0) {
//                if (freq > highest) {
//                    highest = freq;
//                } else if (freq < lowest) {
//                    lowest = freq;
//                } else {}
//            }
//        }


        return highest - lowest;
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        auto answer = solve(inputFilePath);
        return std::to_string(answer);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        std::cout << "ex 2" << std::endl;
        auto answer = solve(inputFilePath, 40);
        return std::to_string(answer);
//        return "";
    }
};

static_assert(DaySolver<Day14>);

#endif //AOC2021_CPP_DAY14_H