#ifndef AOC2021_CPP_GRID_H
#define AOC2021_CPP_GRID_H

#include <iostream>
#include <vector>
#include <ranges>
#include <functional>

#include "fileUtils.h"

namespace ranges = std::ranges;
namespace views = ranges::views;

template<typename T>
class Grid {
public:

    Grid(const uint mSizeR, const uint mSizeC, const T defaultValue) :
            values(std::vector(mSizeR, std::vector<T>(mSizeC, defaultValue))),
            mSizeR(mSizeR),
            mSizeC(mSizeC) {

    }
    Grid(const uint mSizeR, const uint mSizeC) :
            values(std::vector(mSizeR, std::vector<T>(mSizeC))),
            mSizeR(mSizeR),
            mSizeC(mSizeC) {

    }

    /**
     * Returns if the value could be set or not.
     * False if r or c is out of bounds
     */
    inline bool set(int r, int c, T value) {
        if (validCoord(r, c)) {
            auto rt = static_cast<size_t>(r);
            auto ct = static_cast<size_t>(c);
            values[rt][ct] = value;
            return true;
        } else {
            return false;
        }
    }

    inline bool apply(int r, int c, T (*op)(T)) {
        if (validCoord(r, c)) {
            auto rt = static_cast<size_t>(r);
            auto ct = static_cast<size_t>(c);
            values[rt][ct] = op(values[rt][ct]);
            return true;
        } else {
            return false;
        }
    }

    T get(uint r, uint c) const {
        return values[r][c];
    }
    /* UNSAFE */
//    T get(int r, int c) const {
//        return values[r][c];
//    }

    /**
     * Make direction access to the values possible.
     * There is no bound checking in this form
     */
    inline std::vector<T> &operator[](uint r) {
        return values[r];
    }
    /* UNSAFE */
    inline std::vector<T> &operator[](int r) {
        return values[r];
    }

    inline uint getSizeR() const { return mSizeR; }
    inline uint getSizeC() const { return mSizeC; }
    inline uint size() const { return mSizeR * mSizeC; }

    using gridPointHandler = std::function<void(uint r, uint c, T value)>;
    using gridPointModifier = std::function<T(uint r, uint c, T value)>;
//    using gridPointModifierValueOnly = std::function<int(int value)>;

    // Lambda handler
    inline void each(void(*op)(uint r, uint c)) const {
        for (uint r = 0U; r < mSizeR; r++) {
            for (uint c = 0U; c < mSizeC; c++) {
                op(r, c);
            }
        }
    }
    // Lambda value modifier
    inline void eachSave(T(*op)(T value)) {
        for (uint r = 0U; r < mSizeR; r++) {
            for (uint c = 0U; c < mSizeC; c++) {
                values[r][c] = op(values[r][c]);
            }
        }
    }
    // Function handler
    inline void each(const gridPointHandler &op) {
        for (uint r = 0U; r < mSizeR; r++) {
            for (uint c = 0U; c < mSizeC; c++) {
                op(r, c, values[r][c]);
            }
        }
    }
    // Function modifier
    inline void eachSave(const gridPointModifier &op) {
        for (uint r = 0U; r < mSizeR; r++) {
            for (uint c = 0U; c < mSizeC; c++) {
                values[r][c] = op(r, c, values[r][c]);
            }
        }
    }

    inline void eachNeighbor(size_t r, size_t c, bool diagonals, const gridPointHandler &op) {
        std::vector<std::tuple<int, int>> &directions = diagonals ? neighbourDirections
                                                                  : neighbourDirectionsNoDiagonals;
        for (auto[rDir, cDir]: directions) {
            if (validCoord(static_cast<int>(r) + rDir, static_cast<int>(c) + cDir)) {
                auto nr = static_cast<uint>(static_cast<int>(r) + rDir);
                auto nc = static_cast<uint>(static_cast<int>(c) + cDir);
                op(nr, nc, values[nr][nc]);
            }
        }
    }
    inline void eachNeighbor(size_t r, size_t c, bool diagonals, T(*op)(T value)) {
        std::vector<std::tuple<int, int>> &directions = diagonals ? neighbourDirections
                                                                  : neighbourDirectionsNoDiagonals;
        for (auto[rDir, cDir]: directions) {
            if (validCoord(static_cast<int>(r) + rDir, static_cast<int>(c) + cDir)) {
                auto nr = static_cast<uint>(static_cast<int>(r) + rDir);
                auto nc = static_cast<uint>(static_cast<int>(c) + cDir);
                values[nr][nc] = op(values[nr][nc]);
            }
        }
    }

    inline std::vector<std::tuple<int, int>> neighborDirections(bool diagonals) {
        return diagonals ? neighbourDirections
                         : neighbourDirectionsNoDiagonals;
    }

    inline void print() {
        for (const auto &row: values) {
            for (const auto &val: row) {
                std::cout << val << " ";
            }
            std::cout << std::endl;
        }
    }

    template<typename Q, typename F>
    static Grid<Q> fromFile(const std::string &inputFilePath, Q defaultValue, Q(*op)(F)) {
        std::vector<std::string> linesStr = FileUtils::readFileStrings(inputFilePath);
        Grid grid = Grid<Q>(linesStr.size(), linesStr[0U].size(), defaultValue);
        std::vector<std::vector<F>> lines{};
        for (uint r = 0U; r < linesStr.size(); ++r) {
            const auto &line = linesStr[r];
            for (uint c = 0U; c < line.size(); ++c) {
                (void) grid.set(r, c, op(line[c]));
            }
        }
        return grid;
    }

    inline bool validCoord(int r, int c) const {
        return (r >= 0 && static_cast<uint>(r) < mSizeR &&
                c >= 0 && static_cast<uint>(c) < mSizeC);
    }
private:

    std::vector<std::tuple<int, int>> neighbourDirections = {
            {{-1, -1}, {-1, 0}, {-1, 1},
             {0, -1}, {0, 1},
             {1, -1}, {1, 0}, {1, 1}}
    };
    std::vector<std::tuple<int, int>> neighbourDirectionsNoDiagonals = {
            {{-1, 0}, {0, -1}, {0, 1}, {1, 0}}
    };

    std::vector<std::vector<T>> values;
    const uint mSizeR;
    const uint mSizeC;
};

#endif //AOC2021_CPP_GRID_H
