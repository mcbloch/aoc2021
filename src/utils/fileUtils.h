#ifndef AOC2021_CPP_FILEUTILS_H
#define AOC2021_CPP_FILEUTILS_H

#include <algorithm>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <vector>
#include <charconv>

#include "../../inc/color.hpp"

class FileUtils {
public:
    // TODO Rewrite with the use of predicates and less duplicate code

    static std::string readFile(const std::string &filename) {
        std::string line;
        std::ifstream inputFile(filename);

        std::string output;

        if (inputFile.is_open()) {
            while (getline(inputFile, line)) {
                auto s = output.append(line);
            }
            inputFile.close();
        } else {
            std::cout << RED << "Could not find or open input file: " << filename << RESET << std::endl;
        }
        return output;
    }

    static std::vector<std::string> readFileStrings(const std::string &filename) {
        std::string line;
        std::ifstream inputFile(filename);

        std::vector<std::string> output;
        output.reserve(100); // Vague guess of some space

        if (inputFile.is_open()) {
            while (getline(inputFile, line)) {
                output.emplace_back(line);
            }
            inputFile.close();
        } else {
            std::cout << RED << "Could not find or open input file: " << filename << RESET << std::endl;
        }
        return output;
    }

    static std::vector<int> readFileBitstrings(const std::string &filename) {
        std::string line;
        std::ifstream inputFile(filename);

        std::vector<int> output;

        if (inputFile.is_open()) {
            while (getline(inputFile, line)) {
                output.push_back(std::stoi(line, 0, 2));
            }
            inputFile.close();
        } else {
            std::cout << RED << "Could not find or open input file: " << filename << RESET << std::endl;
        }
        return output;
    }


    static std::vector<int> readFileInts(const std::string &filename) {
        std::string line;
        std::ifstream inputFile(filename);

        std::vector<int> nums = {};

        if (inputFile.is_open()) {
            while (getline(inputFile, line)) {
                nums.push_back(std::stoi(line));
            }
            inputFile.close();
        } else {
            std::cout << RED << "Could not find or open input file: " << filename << RESET << std::endl;
        }
        return nums;
    }

    static std::vector<std::string_view>
    splitString(const std::string_view &str, const std::string &separator, int n = 0) {
        size_t startIdx = 0;
        size_t sepIdx;
        std::vector<std::string_view> subs;
        if (n == 0) { n = ((int) str.size() / 2) + 1; }
        subs.reserve(n);

        std::string_view sub;
        do {
            sepIdx = str.find(separator, startIdx);
            sub = str.substr(startIdx, sepIdx - startIdx);
            subs.emplace_back(sub);
            startIdx = sepIdx + (separator.size());
        } while (sepIdx != std::string::npos);
        return subs;
    }

    static std::vector<std::string_view>
    splitString(const std::string_view &str, const char &separator, int n = 0) {
        size_t startIdx = 0;
        size_t sepIdx;
        std::vector<std::string_view> subs;
        if (n == 0) { n = ((int) str.size() / 2) + 1; }
        subs.reserve(n);

        std::string_view sub;
        do {
            sepIdx = str.find(separator, startIdx);
            sub = str.substr(startIdx, sepIdx - startIdx);
            subs.emplace_back(sub);
            startIdx = sepIdx + 1;
        } while (sepIdx != std::string::npos);
        return subs;
    }

    static std::vector<int> splitStringInts(const std::string_view &str, const std::string &separator, int n = 0) {
        size_t startIdx = 0;
        size_t sepIdx;
        std::vector<int> subs;
        if (n == 0) { n = ((int) str.size() / 2) + 1; }
        subs.reserve(n);
        int num;

        std::string_view sub;
        do {
            sepIdx = str.find(separator, startIdx);
            sub = str.substr(startIdx, sepIdx - startIdx);
            (void) std::from_chars(sub.data(), sub.data() + sub.length(), num);
            (void) subs.emplace_back(num);
            startIdx = sepIdx + (separator.size());
        } while (sepIdx != std::string::npos);

        return subs;
    }
};

#endif