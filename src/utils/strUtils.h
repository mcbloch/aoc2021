//
// Created by maxime on 12/16/21.
//

#ifndef AOC2021_CPP_STRUTILS_H
#define AOC2021_CPP_STRUTILS_H

#include <string>
#include <sstream>

static const char *hex_char_to_bin(const char &c) {
    // TODO handle default / error
    switch (toupper(c)) {
        case '0':
            return "0000";
        case '1':
            return "0001";
        case '2':
            return "0010";
        case '3':
            return "0011";
        case '4':
            return "0100";
        case '5':
            return "0101";
        case '6':
            return "0110";
        case '7':
            return "0111";
        case '8':
            return "1000";
        case '9':
            return "1001";
        case 'A':
            return "1010";
        case 'B':
            return "1011";
        case 'C':
            return "1100";
        case 'D':
            return "1101";
        case 'E':
            return "1110";
        case 'F':
            return "1111";
        default:
            return "";
    }
}

#include <bitset>
#include <string>
#include <sstream>
#include <climits>

// note the result is always unsigned
unsigned long bin_str_to_int(std::string const &s) {
    static const std::size_t MaxSize = CHAR_BIT * sizeof(unsigned long);
    if (s.size() > MaxSize) { return 0L; }; // handle error or just truncate?

    std::bitset<MaxSize> bits;
    std::istringstream is(s);
    is >> bits;
    return bits.to_ulong();
}


#endif //AOC2021_CPP_STRUTILS_H
