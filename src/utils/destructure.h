#pragma once

#include <cstdint>
#include <array>
#include <vector>
#include <iterator>
#include <functional>

template<typename T, size_t ... S>
auto destructureImpl(T &t, std::index_sequence<S...>) {
    return std::forward_as_tuple(*std::next(std::begin(t), S)...);
}


template<size_t Count, typename T>
auto destructure(T &t) {
    return destructureImpl(t, std::make_index_sequence<Count>());
}

template<size_t Start, typename T, size_t ... S>
auto destructureImpl(T &t, std::index_sequence<S...>) {
    return std::forward_as_tuple(*std::next(std::begin(t), Start + S)...);
}

template<size_t Start, size_t Count, typename T>
auto destructure(T &t) {
    return destructureImpl<Start>(t, std::make_index_sequence<Count>());
}