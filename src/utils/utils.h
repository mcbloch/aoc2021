#ifndef AOC2021_CPP_UTILS_H
#define AOC2021_CPP_UTILS_H

#include <string>
#include <ostream>
#include <iomanip>

std::string toHex(const std::string &s, bool upperCase = true) {
    std::ostringstream ret;

    for (std::string::size_type i = 0; i < s.length(); ++i) {
        ret << std::hex << std::setfill('0') << std::setw(2) << (upperCase ? std::uppercase : std::nouppercase)
            << (int) s[i];
    }

    return ret.str();
}

#endif //AOC2021_CPP_UTILS_H
