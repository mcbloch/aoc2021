#ifndef AOC2021_CPP_DIJKSTRA_H
#define AOC2021_CPP_DIJKSTRA_H

#include <climits>
#include <algorithm>
#include <functional>
#include <iostream>
#include <string_view>
#include <vector>
#include <set>

using namespace std;

#include "grid.h"

struct Vertex {
    uint r;
    uint c;
    int dist; // Dist
    bool final; //true if vertex is included in shortest path tree or shortest distance from src to vertex is finalized
    bool inQ;
};

struct HeapEntry {
    uint r;
    uint c;
    int dist;
//    bool gravestone = false; // An element can be marked as removed but will stay in the heap;
};
bool operator<(const HeapEntry &c, const HeapEntry &d) {
    return c.r < d.r || c.c < d.c || c.dist < d.dist;
}


struct SmallerDist {
    bool operator()(const HeapEntry &a, const HeapEntry &b) const {
        return a.dist > b.dist;
    }
};

// A utility function to print the constructed distance array
//void printSolution(int dist[]) {
//    cout << "Vertex \t Distance from Source" << endl;
//    for (int i = 0; i < V; i++)
//        cout << i << " \t\t" << dist[i] << endl;
//}

// Function that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
template<typename T>
Grid<Vertex> dijkstra(Grid<T> &graph, uint srcR, uint srcC) {
    // The output array.  dist[i] will hold the shortest distance from src to the coordinate
    Grid<Vertex> dist(graph.getSizeR(), graph.getSizeC());
    dist.eachSave([](uint r, uint c, Vertex v) { return Vertex{r, c, INT_MAX, false, false}; });

//    std::cout << "Item count: " << graph.size() << std::endl;

    // Distance of source vertex from itself is always 0
    dist[srcR][srcC].dist = 0;

    std::vector<HeapEntry> Q{};
    std::set<HeapEntry, std::less<>> blackList{};

    dist[srcR][srcC].inQ = true;
    Q.push_back({srcR, srcC, 0}); // false
    std::ranges::make_heap(Q, SmallerDist());

    // Find shortest path for all vertices
    for (uint count = 0U; count < graph.size() - 1U; count++) {
//        if (count % 10000 == 0) {
//            std::cout << "  at count " << count << std::endl;
//            std::cout << " Qlen: " << Q.size() << std::endl;
//            std::cout << " Blacklistlen: " << blackList.size() << std::endl;
//        }

        HeapEntry min;
        bool tryNext = true;
        while (tryNext) {
            std::ranges::pop_heap(Q, SmallerDist());
            min = Q.back();
            Q.pop_back();
//            if (!min.gravestone) { tryNext = false; }
            if (blackList.contains(min)) {
                blackList.erase(min);
            } else {
                tryNext = false;
            }
        }
        int valU = min.dist;

        // Mark the picked vertex as processed
        dist[min.r][min.c].final = true;

        // Update dist value of the adjacent vertices of the picked vertex.
        auto dirs = dist.neighborDirections(false);
        for (auto[rDir, cDir]: dirs) {
            if (dist.validCoord(static_cast<int>(min.r) + rDir, static_cast<int>(min.c) + cDir)) {
                auto nr = static_cast<uint>(static_cast<int>(min.r) + rDir);
                auto nc = static_cast<uint>(static_cast<int>(min.c) + cDir);

                // Update dist[v] only if is not in final state, there is an edge from
                // u to v, and total weight of path from src to  v through u is
                // smaller than current value of dist[v]
                if (auto newDist = valU + graph[nr][nc];
                        !dist[nr][nc].final &&
                        valU != INT_MAX &&
                        newDist < dist[nr][nc].dist) {
                    // Now mark the old version as discarded and add a new one
                    if (dist[nr][nc].inQ) {
                        (void) blackList.insert({nr, nc, dist[nr][nc].dist});
                        std::cout << "Pushing in blacklist" << std::endl;
                    }
                    dist[nr][nc].dist = newDist;
                    dist[nr][nc].inQ = true;
                    Q.push_back({nr, nc, newDist});
                    std::ranges::push_heap(Q, SmallerDist());
                }
            }
        }

        if (dist[dist.getSizeR() - 1U][dist.getSizeC() - 1U].dist != INT_MAX) {
            break;
        }
    }
    return dist;
}

#endif //AOC2021_CPP_DIJKSTRA_H
