#ifndef AOC2021_CPP_RUNUTILS_H
#define AOC2021_CPP_RUNUTILS_H

#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <chrono>

#include "../../inc/color.hpp"

// Waarom? Statische functies overschrijven is de worst
// Dus steken we die in een concept

template<typename T>
concept DaySolver = requires(T &t){
    { T::idx } ->std::convertible_to<int>;
    { T::ex1("") } ->std::convertible_to<std::string>;
    { T::ex2("") } ->std::convertible_to<std::string>;
};

struct DayInputSet {
    std::string inputFile;
    std::string solution1;
    std::string solution2;
};

struct SolveResult {
    int part;
    std::string answer;
    std::string solution;
    long runTime; // In microseconds
};


template<DaySolver D>
void runDayPretty(DayInputSet dayInputSet) {
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::duration;
    using std::chrono::microseconds;

    std::cout << "---- Running Day " << BOLDMAGENTA << D::idx << RESET << " ----" << std::endl;

    auto t1 = high_resolution_clock::now();
    std::string ans1 = D::ex1(dayInputSet.inputFile);
    auto t2 = high_resolution_clock::now();
    std::string ans2 = D::ex2(dayInputSet.inputFile);
    auto t3 = high_resolution_clock::now();

    /* Getting number of nanoseconds as an integer. */
    auto nsInt1 = duration_cast<microseconds>(t2 - t1).count();
    auto nsInt2 = duration_cast<microseconds>(t3 - t2).count();

    for (const auto &solveResult: std::vector<SolveResult>{
            {1, ans1, dayInputSet.solution1, nsInt1},
            {2, ans2, dayInputSet.solution2, nsInt2}
    }) {
        if (!solveResult.solution.empty()) {
            if (solveResult.answer == solveResult.solution) {
                std::cout <<
                          BRIGHTGREEN << "✔ " << RESET <<
                          "Exercise " << solveResult.part << ": " <<
                          GREEN << solveResult.answer << RESET <<
                          DIM << "\t\t (in " << solveResult.runTime << " ŋs)" << RESET <<
                          std::endl;
            } else {
                std::cout <<
                          RED << "✘ " << RESET <<
                          "Exercise " << solveResult.part << ": Got " <<
                          RED << (!solveResult.answer.empty() ? solveResult.answer : "nothing") << RESET <<
                          ", should be " <<
                          BLUE + solveResult.solution << RESET <<
                          DIM << "\t (in " << solveResult.runTime << " ŋs)" << RESET <<
                          std::endl;
            }
        } else {
            std::cout <<
                      BLINKCYAN << "➖ " << RESET <<
                      "Exercise " << solveResult.part << ": " <<
                      CYAN << solveResult.answer << RESET <<
                      DIM << "\t\t (in " << solveResult.runTime << " ŋs)" << RESET <<
                      std::endl;
        }
    }
    std::cout << std::endl;
}

#endif //AOC2021_CPP_RUNUTILS_H