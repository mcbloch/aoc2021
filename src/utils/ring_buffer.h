#ifndef AOC2021_CPP_RING_BUFFER_H
#define AOC2021_CPP_RING_BUFFER_H

#include <array>
#include <iostream>

using namespace std;

typedef unsigned int uint32;

template<class T, int mSize>
class RingBuffer {

private:
    uint32 mPosition;
    std::array<T, mSize> mValues;

public:
    // Constructor
    RingBuffer();

    // Destructor
    ~RingBuffer() = default;

    // Iterator class
    class iterator;

    // Add element to Ring Buffer
    void add(T value);

    // Get size of the Ring Buffer
    uint32 size() const;

    // Get element at specific position
    // Return type of get method is reference
    // because all the STL containers are
    // returning reference only
    T &get(uint32 position);

    T &operator[](uint32 position);

    // Progress ringbuffer pointer
    void operator++();

    // begin() function
    iterator begin() {
        return iterator(0, *this);
    }

    // end() function
    iterator end() {
        return iterator(mSize, *this);
    }

    std::array<T, mSize> &data() {
        return mValues;
    }

};

template<class T, int mSize>
class RingBuffer<T, mSize>::iterator {

private:
    uint32 mPosition;
    RingBuffer &mRingBuffer;

public:
    // Constructor
    iterator(uint32 position, RingBuffer ringBuffer) : mPosition(0), mRingBuffer(ringBuffer) {
    }

    // Overload Operator Postfix ++
    iterator operator++(int) {
        mPosition++;
        return *this;
    }

    // Overload operator prefix ++
    iterator &operator++() {
        mPosition++;
        return *this;
    }

    // Overload Operator de-reference operator
    T &operator*() {
        return mRingBuffer.get(mPosition);
    }

    // Overload operator !=
    bool operator!=(const iterator &other) const {
        return (mPosition != other.mPosition);
    }
};

template<class T, int mSize>
RingBuffer<T, mSize>::RingBuffer() :
        mPosition(0), mValues(std::array<T, mSize>{}) {
}

template<class T, int mSize>
void RingBuffer<T, mSize>::add(T value) {
    mValues[mPosition++] = value;
    if (mPosition == mSize) {
        mPosition = 0;
    }
}

template<class T, int mSize>
uint32 RingBuffer<T, mSize>::size() const {
    return mSize;
}

template<class T, int mSize>
T &RingBuffer<T, mSize>::get(uint32 position) {
    return mValues[(mPosition + position) % mSize];
}

template<class T, int mSize>
T &RingBuffer<T, mSize>::operator[](uint32 position) {
    return mValues[(mPosition + position) % mSize];
}

template<class T, int mSize>
void RingBuffer<T, mSize>::operator++() {
    mPosition++;
}

#endif //AOC2021_CPP_RING_BUFFER_H
