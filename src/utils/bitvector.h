//
// Created by maxime on 12/15/21.
//

#ifndef AOC2021_CPP_BITVECTOR_H
#define AOC2021_CPP_BITVECTOR_H

#include <array>

/**
     * A BitVector supporting infinite size
     * The size of the vector is 64 bit * I
     */
template<int I>
struct BitVector {
    std::array<long long, I> bits{0};

    // Sets bit to 1
    void setBit(size_t i) {
        size_t ri = i / 64U;
        size_t ci = i % 64U;
        bits[ri] |= (1 << ci);
    }

    // Returns if bit is set
    bool getBit(size_t i) {
        size_t ri = i / 64U;
        size_t ci = i % 64U;
        return (bits[ri] & (1 << ci)) != 0;
    }
};

#endif //AOC2021_CPP_BITVECTOR_H
