#ifndef AOC2021_CPP_DAY01_H
#define AOC2021_CPP_DAY01_H

#include <string>
#include <utility>
#include <vector>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

#define GET_INT_LINE(stream, line, var)  getline(stream, line); (var) = std::stoi(line)

struct Day01 {
    static const int idx = 1;

    static inline std::string ex1(const std::string &inputFilePath) {
        std::vector<int> nums = FileUtils::readFileInts(inputFilePath);

        unsigned int increaseCount = 0;
        for (size_t i = 1; i < nums.size(); i++) {
            if (nums[i] > nums[i - 1]) {
                increaseCount++;
            }
        }
        return std::to_string(increaseCount);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        if (std::ifstream inputStream(inputFilePath); inputStream.is_open()) {
            int increaseCount = 0;
            std::string line;

            int a, b, c, d;
            GET_INT_LINE(inputStream, line, a);
            GET_INT_LINE(inputStream, line, b);
            GET_INT_LINE(inputStream, line, c);

            while (getline(inputStream, line)) {
                d = std::stoi(line);

                if (d > a) {
                    increaseCount++;
                }
                a = b;
                b = c;
                c = d;
            }
            inputStream.close();
            return std::to_string(increaseCount);
        } else {
            return "Could not find or open input file.";
        }
    }
};

static_assert(DaySolver<Day01>);

#endif //AOC2021_CPP_DAY01_H