#include <array>
#include <functional>

#include "interpreter/codeGenerator.h"
#include "day01.h"
#include "day02.h"
#include "day03.h"
#include "day04.h"
#include "day05.h"
#include "day06.h"
#include "day07.h"
#include "day08.h"
#include "day09.h"
#include "day10.h"
#include "day11.h"
#include "day12.h"
#include "day13.h"
#include "day14.h"
#include "day15.h"
#include "day16.h"
#include "day17.h"
#include "day18.h"


using exFType = std::function<std::string(std::string)>;

int main(int argc, char *argv[]) {
//    std::string res1 = PythonGenerator().generateAndRun("../res/input_2.txt");
//    std::string res2 = RubyGenerator().generateAndRun("../res/input_2.txt");
//    std::string res3 = CppGenerator().generateAndRun("../res/input_2.txt");
//    std::string res4 = CGenerator().generateAndRun("../res/input_2.txt");
//    std::string res5 = ClojureGenerator().generateAndRun("../res/input_2.txt");
//    std::string res6 = GoGenerator().generateAndRun("../res/input_2.txt");

//    std::string res = Interpreter().interpretFile("../res/input_2.txt");

//    std::cout << res << std::endl;
//    exit(0);

    if (argc > 1) {
        // Run a specific part
        size_t day = static_cast<size_t>(std::stoi(argv[1]));
        size_t part = static_cast<size_t>(std::stoi(argv[2]));
        std::string file = argv[3];

        std::cout <<
                  std::array<std::array<exFType, 2U>, 30U>{
                          {
                                  {&Day01::ex1, &Day01::ex2}, {&Day02::ex1, &Day02::ex2},
                                  {&Day03::ex1, &Day03::ex2}, {&Day04::ex1, &Day04::ex2},
                                  {&Day05::ex1, &Day05::ex2}, {&Day06::ex1, &Day06::ex2},
                                  {&Day07::ex1, &Day07::ex2}, {&Day08::ex1, &Day08::ex2},
                                  {&Day09::ex1, &Day09::ex2}, {&Day10::ex1, &Day10::ex2},
                                  {&Day11::ex1, &Day11::ex2}, {&Day12::ex1, &Day12::ex2},
                                  {&Day13::ex1, &Day13::ex2}, {&Day14::ex1, &Day14::ex2},
                                  {&Day15::ex1, &Day15::ex2}, {&Day16::ex1, &Day16::ex2},
                                  {&Day17::ex1, &Day17::ex2}, {&Day18::ex1, &Day18::ex2}
                          }
                  }[day - 1][part - 1](file)
                  << std::endl;
    } else {
        // Just run everything
        auto defaultD1 = DayInputSet{"../res/input_1.txt", "1696", "1737"};
        auto defaultD2 = DayInputSet{"../res/input_2.txt", "1635930", "1781819478"};
        auto defaultD3 = DayInputSet{"../res/input_3.txt", "3009600", "6940518"};
        auto defaultD4Ex = DayInputSet{"../res/input_4_example.txt", "4512", "1924"};
        auto defaultD4 = DayInputSet{"../res/input_4.txt", "4662", "12080"};
        auto defaultD5Ex = DayInputSet{"../res/input_5_example.txt", "5", "12"};
        auto defaultD5 = DayInputSet{"../res/input_5.txt", "4728", "17717"};
        auto defaultD6Ex = DayInputSet{"../res/input_6_example.txt", "5934", "26984457539"};
        auto defaultD6 = DayInputSet{"../res/input_6.txt", "350917", "1592918715629"};
        auto defaultD7Ex = DayInputSet{"../res/input_7_example.txt", "37", "168"};
        auto defaultD7 = DayInputSet{"../res/input_7.txt", "344535", "95581659"};
        auto defaultD8Ex = DayInputSet{"../res/input_8_example.txt", "26", "61229"};
        auto defaultD8 = DayInputSet{"../res/input_8.txt", "245", "983026"};
        auto defaultD9Ex = DayInputSet{"../res/input_9_example.txt", "15", "1134"};
        auto defaultD9 = DayInputSet{"../res/input_9.txt", "600", "987840"};
        auto defaultD10 = DayInputSet{"../res/input_10.txt"};
        auto defaultD11Ex = DayInputSet{"../res/input_11_example.txt", "1656", "195"};
        auto defaultD11 = DayInputSet{"../res/input_11.txt", "1719", "232"};
        auto defaultD12Ex1 = DayInputSet{"../res/input_12_ex1.txt", "10", "36"};
        auto defaultD12Ex2 = DayInputSet{"../res/input_12_ex2.txt", "19", "103"};
        auto defaultD12Ex3 = DayInputSet{"../res/input_12_ex3.txt", "226", "3509"};
        auto defaultD12 = DayInputSet{"../res/input_12.txt", "3463", "91533"};
        auto defaultD13Ex = DayInputSet{"../res/input_13_example.txt", "17"};
        auto defaultD13 = DayInputSet{"../res/input_13.txt", "631", "EFLFJGRF"};
        auto defaultD14Ex = DayInputSet{"../res/input_14_example.txt", "1588", "2188189693529"};
        auto defaultD14 = DayInputSet{"../res/input_14.txt", "2375", ""};
        auto defaultD15Ex = DayInputSet{"../res/input_15_example.txt", "40", "315"};
        auto defaultD15 = DayInputSet{"../res/input_15.txt", "702", "2955"};
        auto defaultD16Ex1 = DayInputSet{"../res/input_16_ex1.txt", "16"};
        auto defaultD16Ex2 = DayInputSet{"../res/input_16_ex2.txt", "12"};
        auto defaultD16Ex3 = DayInputSet{"../res/input_16_ex3.txt", "23"};
        auto defaultD16Ex4 = DayInputSet{"../res/input_16_ex4.txt", "31"};
        auto defaultD16 = DayInputSet{"../res/input_16.txt", "967", "12883091136209"};
        auto defaultD17 = DayInputSet{"../res/input_17.txt", "6441", "3186"};
        auto defaultD18Ex = DayInputSet{"../res/input_18_example.txt", "4140", "3993"};
        auto defaultD18 = DayInputSet{"../res/input_18.txt", "4202", "4779"};
//        runDayPretty<Day01>(defaultD1);
//        runDayPretty<Day02>(defaultD2);
//        runDayPretty<Day03>(defaultD3);
//        runDayPretty<Day04>(defaultD4Ex);
//        runDayPretty<Day04>(defaultD4);
//        runDayPretty<Day05>(defaultD5Ex);
//        runDayPretty<Day05>(defaultD5);
//        runDayPretty<Day06>(defaultD6Ex);
//        runDayPretty<Day06>(defaultD6);
//        runDayPretty<Day07>(defaultD7Ex);
//        runDayPretty<Day07>(defaultD7);
//        runDayPretty<Day08>(defaultD8Ex);
//        runDayPretty<Day08>(defaultD8);
//        runDayPretty<Day09>(defaultD9Ex);
//        runDayPretty<Day09>(defaultD9);
//        runDayPretty<Day10>(defaultD10); // TODO
//        runDayPretty<Day11>(defaultD11Ex);
//        runDayPretty<Day11>(defaultD11);
//        runDayPretty<Day12>(defaultD12Ex1);
//        runDayPretty<Day12>(defaultD12Ex2);
//        runDayPretty<Day12>(defaultD12Ex3);
//        runDayPretty<Day12>(defaultD12);
//        runDayPretty<Day13>(defaultD13Ex);
//        runDayPretty<Day13>(defaultD13);
//        runDayPretty<Day14>(defaultD14Ex);
//        runDayPretty<Day14>(defaultD14); // TODO
//        runDayPretty<Day15>(defaultD15Ex);
        runDayPretty<Day15>(defaultD15); // Slow :p
//        runDayPretty<Day16>(defaultD16Ex1);
//        runDayPretty<Day16>(defaultD16Ex2);
//        runDayPretty<Day16>(defaultD16Ex3);
//        runDayPretty<Day16>(defaultD16Ex4); // TODO Add a way to test individual parts, not a whole day
//        runDayPretty<Day16>(defaultD16);
//        runDayPretty<Day17>(defaultD17);
//        runDayPretty<Day18>(defaultD18Ex);
//        runDayPretty<Day18>(defaultD18);
    }
    return 0;
}
