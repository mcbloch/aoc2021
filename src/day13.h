#ifndef AOC2021_CPP_DAY13_H
#define AOC2021_CPP_DAY13_H

#include <string>
#include <utility>
#include <vector>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/destructure.h"

struct Day13 {
    static const int idx = 13;

    typedef std::vector<std::vector<bool>> grid;

    static void printGrid(const grid &grid) {
        std::cout << "----- Grid -----" << std::endl;
        for (const auto &row: grid) {
            for (auto cell: row) {
                if (cell) {
                    std::cout << '#';
                } else {
                    std::cout << '.';
                }
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    static grid gridFold(const std::string &inputFilePath, bool singleFold = true) {
        std::string line;
        std::ifstream inputFile(inputFilePath);

        size_t x_size = 0;
        size_t y_size = 0;

        std::vector<std::tuple<int, int>> dots{};
        std::vector<std::tuple<char, int>> instructions{};

        bool empty_passed = false;
        if (inputFile.is_open()) {
            while (getline(inputFile, line)) {
                if (line.empty()) {
                    empty_passed = true;
                } else {
                    if (!empty_passed) {
                        auto data = FileUtils::splitStringInts(line, ",");
                        auto[x, y] = destructure<2, std::vector<int>>(data);
                        if (x >= x_size) { x_size = x + 1; }
                        if (y >= y_size) { y_size = y + 1; }
                        (void) dots.emplace_back(std::make_tuple(x, y));
                    } else {
                        auto parts = FileUtils::splitString(line, " ");
                        auto val = FileUtils::splitString(parts[2], "=");
                        int v;
                        (void) std::from_chars(val[1].data(), val[1].data() + val[1].length(), v);

                        (void) instructions.emplace_back(std::make_tuple(val[0][0], v));
                    }
                }
            }
            inputFile.close();
        } else {
            std::cout << RED << "Could not find or open input file: " << inputFilePath << RESET << std::endl;
        }

        std::vector<std::vector<bool>> grid(
                y_size,
                std::vector<bool>(x_size));

        for (auto[x, y]: dots) {
            grid[y][x] = true;
        }

        for (auto[i_axis, i_value]: instructions) {
            if (i_axis == 'x') {
                for (int y = 0; y < y_size; y++) {
                    for (int row_i = 1; row_i <= i_value; row_i++) {
                        // -- 7; 8 -> 6
                        // -- 7; 9 -> 5
                        if (i_value - row_i < 0 || i_value + row_i >= x_size) { break; }
                        grid[y][i_value - row_i] = grid[y][i_value + row_i] || grid[y][i_value - row_i];
                    }
                }
                x_size = i_value;
                for (auto &row: grid) {
                    row.resize(x_size);
                }
            } else if (i_axis == 'y') {
                for (int x = 0; x < x_size; x++) {
                    for (int col_i = 1; col_i <= i_value; col_i++) {
                        // -- 7; 8 -> 6
                        // -- 7; 9 -> 5
                        if (i_value - col_i < 0 || i_value + col_i >= y_size) { break; }
                        grid[i_value - col_i][x] = grid[i_value + col_i][x] || grid[i_value - col_i][x];
                    }
                }
                y_size = i_value;
                grid.resize(y_size);
            } else {
                std::cout << "Program looks at you in a confused way." << std::endl;
            }
            if (singleFold) {
                break;
            }
        }
        return grid;
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        const grid grid = gridFold(inputFilePath);
        int sum = 0;
        for (const auto &row: grid) {
            for (const auto &cell: row) {
                if (cell) {
                    sum++;
                }
            }
        }

        return std::to_string(sum);
    }

    static inline std::string
    ex2(const std::string &inputFilePath) {
        const grid grid = gridFold(inputFilePath, false);
        printGrid(grid);
        return "EFLFJGRF";
    }
};

static_assert(DaySolver<Day13>);

#endif //AOC2021_CPP_DAY13_H