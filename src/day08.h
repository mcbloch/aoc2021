#ifndef AOC2021_CPP_DAY08_H
#define AOC2021_CPP_DAY08_H

#include <cstdint>
#include <ranges>
#include <numeric>
#include <string>
#include <utility>
#include <bits/stdc++.h>
#include <iostream>

#include "utils/fileUtils.h"
#include "utils/destructure.h"
#include "utils/runUtils.h"

struct Day08 {

    typedef unsigned char Mask;

    static const int idx = 8;

    struct LengthToLeftMasks {
        Mask two{};
        Mask three{};
        Mask four{};
        std::array<Mask, 3> five{};
        int fiveIdx = 0;
        std::array<Mask, 3> six{};
        int sixIdx = 0;
        Mask seven{};

        void addMask(int numberSize, Mask mask) {
            if (numberSize == 2) { two = mask; }
            if (numberSize == 3) { three = mask; }
            if (numberSize == 4) { four = mask; }
            if (numberSize == 7) { seven = mask; }
            if (numberSize == 5) {
                five[fiveIdx] = mask;
                fiveIdx++;
            }
            if (numberSize == 6) {
                six[sixIdx] = mask;
                sixIdx++;
            }
        }
    };


    static int countSetBits(Mask m) {
        return __builtin_popcount(m);
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        int sum = 0;
        for (const auto &line : FileUtils::readFileStrings(inputFilePath)) {
            auto lr = FileUtils::splitString(line, " | ", 2);

            auto right = lr[1];
            auto rightParts = FileUtils::splitString(right, ' ', 4);

            int one = 2;
            int four = 4;
            int seven = 3;
            int eight = 7;

            for (auto part : rightParts) {
                int s = (int) part.size();
                if (s == one || s == four || s == seven || s == eight) {
                    sum++;
                }
            }
        }

        return std::to_string(sum);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        int sum = 0;

        for (const auto &line : FileUtils::readFileStrings(inputFilePath)) {
            auto lr = FileUtils::splitString(line, " | ", 2);
            auto[left, right] = destructure<2, std::vector<std::string_view>>(lr);

            std::vector<std::string_view> leftPartsString = FileUtils::splitString(left, ' ', 4);

            // A vector of masks that say which character is in a string with the following mapping
            //     Ob ? ? ? ? ? ? ?
            //        | | | | | | |
            //        g f e d c b a
            // The array is not in the correct order
            // Length of numberString to a vector of the masks of numbers with that length
            LengthToLeftMasks lengthToLeftMasks{};
            for (int i = 0; i < 10; i++) {
                std::string_view part = leftPartsString[i];
                Mask mask = 0;
                for (char c : part) {
                    int characterIdx = (c - 'a');
                    int characterMask = (1 << characterIdx);
                    mask |= characterMask;
                }
                lengthToLeftMasks.addMask((int)part.size(), mask);
            }

            std::array<Mask, 10> numberToCharacterMask{}; // <- Our variable we need to resolve

            // Set the numbers with known lengths
            numberToCharacterMask[1] = lengthToLeftMasks.two;
            numberToCharacterMask[4] = lengthToLeftMasks.four;
            numberToCharacterMask[7] = lengthToLeftMasks.three;
            numberToCharacterMask[8] = lengthToLeftMasks.seven;

            // Subtract 1 and 4 masks and check the amount of characters left. These are identifying
            for (auto leftMask : lengthToLeftMasks.five) {
                if (countSetBits(leftMask & ~numberToCharacterMask[4]) == 3) {
                    numberToCharacterMask[2] = leftMask;
                } else if (countSetBits(leftMask & ~numberToCharacterMask[1]) == 3) {
                    numberToCharacterMask[3] = leftMask;
                } else {
                    numberToCharacterMask[5] = leftMask;
                }
            }
            for (auto leftMask : lengthToLeftMasks.six) {
                if (countSetBits(leftMask & ~numberToCharacterMask[4]) == 2) {
                    numberToCharacterMask[9] = leftMask;
                } else if (countSetBits(leftMask & ~numberToCharacterMask[1]) == 5) {
                    numberToCharacterMask[6] = leftMask;
                } else {
                    numberToCharacterMask[0] = leftMask;
                }
            }

            // ------
            // Now we have a valid mapping of our letters to our signals
            // We have the letterMasks for each number 0 -> 9
            // Every number on the right will match on one of the given masks.

            auto rightParts = FileUtils::splitString(right, ' ');
            std::string lineNumberRes;

            for (auto part : rightParts) {
                Mask mask = 0;
                for (char c : part) {
                    int characterIdx = (c - 'a');
                    int characterMask = (1 << characterIdx);
                    mask |= characterMask;
                }

                auto numberMatchIt = std::ranges::find(numberToCharacterMask, mask);
                size_t number = numberMatchIt - numberToCharacterMask.begin();
                lineNumberRes.append(std::to_string(number));
            }

            int lineNumber = std::stoi(lineNumberRes);
            sum += lineNumber;
        }

        return std::to_string(sum);
    }
};

static_assert(DaySolver<Day08>);


#endif //AOC2021_CPP_DAY08_H