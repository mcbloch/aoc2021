#ifndef AOC2021_CPP_DAY03_H
#define AOC2021_CPP_DAY03_H

#include <iostream>
#include <numeric>
#include <string_view>
#include <vector>
#include <bitset>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Day03 {
    static const int idx = 3;

    static inline std::string ex1(const std::string &inputFilePath) {
        std::vector<int> lines = FileUtils::readFileBitstrings(inputFilePath);

        int gamma = 0;
        int eps = 0;

        for (int i = 0; i < 12; i++) {
            size_t oneCount = 0;
            for (auto line : lines) {
                if ((line & (1 << i)) != 0) { oneCount++; }
            }

            if (oneCount > lines.size() - oneCount) {
                gamma = (gamma | (1 << i));
            } else {
                eps = (eps | (1 << i));
            }
        }
        return std::to_string(gamma * eps);
    }

    static int calculateRating(std::vector<int> lines, bool inverse = false) {
        for (int i = 11; i >= 0; i--) {

            size_t oneCount = 0;
            for (auto line : lines) {
                if ((line & (1 << i)) != 0) { oneCount++; }
            }
            int filterValue = (oneCount >= lines.size() - oneCount) == !inverse;

            (void) std::erase_if(lines, [&](int line) {
                return ((line & (1 << i)) != (filterValue << i));
            });

            if (lines.size() <= 1) {
                break;
            }
        }
        return lines[0];
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        std::vector<int> lines = FileUtils::readFileBitstrings(inputFilePath);

        int gamma = calculateRating(lines);
        int eps = calculateRating(lines, true);

        return std::to_string(gamma * eps);
    }
};

static_assert(DaySolver<Day03>);

#endif //AOC2021_CPP_DAY03_H
