#ifndef AOC2021_CPP_DAY17_H
#define AOC2021_CPP_DAY17_H

#include <string>
#include <vector>
#include <regex>
#include <utility>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Day17 {
    struct point {
        int x;
        int y;
    };
    struct vector {
        int x;
        int y;
    };
    struct Area {
        point topLeft;
        point bottomRight;
    };

    static const int idx = 17;

    static std::tuple<bool, int> simulateShot(vector velocity, Area targetArea) {
        // If not in area yet.
        point probePosition = {0, 0};
        int highestY = 0;

        bool search = true;
        while (search) {
            if (probePosition.x > targetArea.bottomRight.x || (
                    velocity.x == 0 && probePosition.y < targetArea.bottomRight.y
            )) {
                return {false, highestY};
            }
            if (probePosition.x >= targetArea.topLeft.x &&
                probePosition.x <= targetArea.bottomRight.x &&
                probePosition.y <= targetArea.topLeft.y &&
                probePosition.y >= targetArea.bottomRight.y) {
                return {true, highestY};
            }

            probePosition.x += velocity.x;
            probePosition.y += velocity.y;
            if (velocity.x > 0) { velocity.x -= 1; }
            if (velocity.x < 0) { velocity.x += 1; }
            velocity.y -= 1;

            if (probePosition.y > highestY) { highestY = probePosition.y; }
        }
    }

    static inline Area readArea(const std::string &inputFilePath) {
        auto str = FileUtils::readFileStrings(inputFilePath);
        auto line = str[0U];

        std::regex area_regex(R"(^target area: x=(\d+)\.\.(\d+), y=(-?\d+)\.\.(-?\d+)$)");
        std::smatch m;
        std::vector<std::string> strs(4u);
        bool match = regex_match(line, m, area_regex);

        int xAreaFrom = std::stoi(m[1].str());
        int xAreaTo = std::stoi(m[2].str());
        int yAreaTo = std::stoi(m[3].str());
        int yAreaFrom = std::stoi(m[4].str());

        return {
                {xAreaFrom, yAreaFrom},
                {xAreaTo,   yAreaTo}
        };
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        Area area = readArea(inputFilePath);

//         for every x, for every y, simulate and break if miss
        int horizontalDistanceToTravel = area.topLeft.x;
        int highestY = 0;
        for (int i = 0; i < area.bottomRight.x; i++) {
            for (int j = -1000; j < 1000; j++) {
                auto[res, highestYLocal] = simulateShot({i, j}, area);
                if (res) {
                    if (highestYLocal > highestY) { highestY = highestYLocal; }
                }
            }
        }
        return std::to_string(highestY);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        Area area = readArea(inputFilePath);

//         for every x, for every y, simulate and break if miss
        int horizontalDistanceToTravel = area.topLeft.x;
        int countValid = 0;
        for (int i = 0; i <= area.bottomRight.x; i++) {
            for (int j = -1000; j < 1000; j++) {
                auto[res, highestYLocal] = simulateShot({i, j}, area);
                if (res) {
                    countValid++;
                }
            }
        }
        return std::to_string(countValid);
    }
};

static_assert(DaySolver<Day17>);

#endif //AOC2021_CPP_DAY17_H