#ifndef AOC2021_CPP_DAY15_H
#define AOC2021_CPP_DAY15_H

#include <string>
#include <vector>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/grid.h"
#include "utils/dijkstra.h"

struct Day15 {
    static const int idx = 15;

    static inline std::string ex1(const std::string &inputFilePath) {
        Grid grid = Grid<int>::fromFile<int, int>(inputFilePath, -1, [](int ch) { return ch - '0'; });
        auto x = dijkstra(grid, 0U, 0U);
//        x.print();
        return std::to_string(x.get(x.getSizeR() - 1, x.getSizeC() - 1).dist);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        Grid<int> grid = Grid<int>::fromFile<int, int>(inputFilePath, -1, [](int ch) { return ch - '0'; });
        Grid<int> realGrid = Grid<int>(grid.getSizeR() * 5, grid.getSizeC() * 5, -1);

        for (uint i = 0U; i < 5U; ++i) {
            for (uint j = 0U; j < 5U; ++j) {
                grid.each([&](uint r, uint c, int val) {
                    (void) realGrid.set((grid.getSizeR() * i) + r, (grid.getSizeC() * j) + c,
                                        (((i + j + val) - 1U) % 9U) + 1U);
                });
            }
        }

        auto x = dijkstra(realGrid, 0U, 0U);
        return std::to_string(x.get(x.getSizeR() - 1U, x.getSizeC() - 1U).dist);
    }
};

static_assert(DaySolver<Day15>);

#endif //AOC2021_CPP_DAY15_H