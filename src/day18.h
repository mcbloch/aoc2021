#ifndef AOC2021_CPP_DAY18_H
#define AOC2021_CPP_DAY18_H

#include <string>
#include <vector>
#include <list>
#include <memory>
#include <cmath>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Day18 {
    static const int idx = 18;


    struct Pair {
        uint depth;

        std::shared_ptr<Pair> left = nullptr;
        std::shared_ptr<Pair> right = nullptr;

        long value = -1;

        std::string toString() {
            if (value != -1) { return std::to_string(value); }
            else {
                return "( " +
                       left->toString() + ", " +
                       right->toString() + " )";
            }
        }

        void incrementDepth() {
            depth++;
            if (left != nullptr) { left->incrementDepth(); }
            if (right != nullptr) { right->incrementDepth(); }
        }

        std::shared_ptr<Pair> findPairWithDepth(uint d) {
            if (left->value == -1) {
                if (left->depth == d) {
                    return left;
                } else {
                    if (auto nested = left->findPairWithDepth(d); nested != nullptr) {
                        return nested;
                    }
                }
            }
            if (right->value == -1) {
                if (right->depth == d) {
                    return right;
                } else {
                    if (auto nested = right->findPairWithDepth(d); nested != nullptr) {
                        return nested;
                    }
                }
            }
            return nullptr;
        }


        long getMagnitude() {
            if (value != -1) { return value; }
            else {
                return 3 * left->getMagnitude() + 2 * right->getMagnitude();
            }
        }
    };

    static inline std::shared_ptr<Pair> parsePair(const std::string &line, const uint startIdx, const uint endIdx,
                                                  const uint depthStart,
                                                  std::list<std::shared_ptr<Pair>> &literalList) {
        uint depth = depthStart;
        uint splitIdx;
        bool literal = true;
        for (uint j = startIdx; j < endIdx; j++) {
            char c = line[j];
            if (c == '[') { depth++; }
            else if (c == ']') { depth--; }
            else {}

            if (depth == depthStart + 1U && c == ',') {
                splitIdx = j;
                literal = false;
            }
            if (depth == depthStart) {
                break;
            }
        }
        auto pair = std::make_shared<Pair>();
        pair->depth = depthStart;
        if (literal) {
            int num = std::stoi(line.substr(startIdx, endIdx - startIdx));
            pair->value = num;
            literalList.push_back(pair);
        } else {
            pair->left = parsePair(line, startIdx + 1U, splitIdx, depthStart + 1U, literalList);
            pair->right = parsePair(line, splitIdx + 1U, endIdx, depthStart + 1U, literalList);
        }

        return pair;
    }

    static inline bool
    doOneEvaluation(const std::shared_ptr<Pair> &rootPair, std::list<std::shared_ptr<Pair>> &literalList) {
        // Find 4 depth
        std::shared_ptr<Pair> currentPair = rootPair->findPairWithDepth(4U);
        if (currentPair != nullptr) {
//            std::cout << "Expand: " << currentPair->toString() << std::endl;
            const auto lValueIt = std::ranges::find(literalList, currentPair->left);
            if (lValueIt != literalList.begin()) {
                (*(std::prev(lValueIt)))->value += currentPair->left->value;
            }

            auto rValueIt = lValueIt;
            auto rValueNextIt = lValueIt;
            ++rValueIt;
            ++rValueNextIt;
            ++rValueNextIt;

            if (rValueNextIt != literalList.end()) {
                (*rValueNextIt)->value += currentPair->right->value;
            }
            currentPair->value = 0;
            currentPair->left = nullptr;
            currentPair->right = nullptr;

            *lValueIt = currentPair;
            literalList.erase(rValueIt);

            return true;
        }

        // Find num bigger then 10
        const auto currentLiteralIt = std::ranges::find_if(literalList, [](const std::shared_ptr<Pair> &p) {
            return p->value >= 10;
        });
        if (currentLiteralIt != literalList.end()) {
            std::shared_ptr<Pair> currentLiteral = *currentLiteralIt;
//            std::cout << "Split: " << currentLiteral->toString() << std::endl;

            auto pair1 = std::make_shared<Pair>();
            auto pair2 = std::make_shared<Pair>();

            currentLiteral->left = pair1;
            pair1->depth = currentLiteral->depth + 1U;
            pair1->value = static_cast<long>(floor(currentLiteral->value / 2.f));

            currentLiteral->right = pair2;
            pair2->depth = currentLiteral->depth + 1U;
            pair2->value = static_cast<long>(ceil(currentLiteral->value / 2.f));

            currentLiteral->value = -1;

            *currentLiteralIt = pair1;
            auto nextLiteralIt = currentLiteralIt;
            ++nextLiteralIt;
            literalList.insert(nextLiteralIt, pair2);

            return true;
        }

        return false;
    }
    static inline std::shared_ptr<Pair>
    addPair(const std::shared_ptr<Pair> &pair1,
            const std::shared_ptr<Pair> &pair2,
            std::list<std::shared_ptr<Pair>> &horiz1,
            const std::list<std::shared_ptr<Pair>> &horiz2) {
        auto pair = std::make_shared<Pair>();

        pair1->incrementDepth();
        pair2->incrementDepth();

        pair->depth = 0U;
        pair->left = pair1;
        pair->right = pair2;

        for (const auto &x: horiz2) {
            horiz1.push_back(x);
        }

        return pair;
    }

    static inline void printHoriz(const std::list<std::shared_ptr<Pair>> &literalList) {
        for (auto &literal: literalList) {
            std::cout << literal->value << ", ";
        }
        std::cout << std::endl;
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        auto lines = FileUtils::readFileStrings(inputFilePath);
        std::list<std::shared_ptr<Pair>> literalList{};
        auto root = parsePair(lines[0U], 0U, lines[0U].size(), 0U, literalList);

//        std::cout << root->toString() << std::endl;
//        printHoriz();

        for (uint i = 1U; i < lines.size(); i++) {
            std::list<std::shared_ptr<Pair>> horizNew{};
            auto next = parsePair(lines[i], 0U, lines[i].size(), 0U, horizNew);
            root = addPair(root, next, literalList, horizNew);
//            std::cout << root->toString() << std::endl;

            bool didEvaluation = true;
            while (didEvaluation) {
                didEvaluation = doOneEvaluation(root, literalList);
//                std::cout << root->toString() << std::endl;
//                printHoriz(literalList);
            }
        }

        return std::to_string(root->getMagnitude());
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        auto lines = FileUtils::readFileStrings(inputFilePath);
        long biggestMagnitude = 0;
        for (uint i = 0U; i < lines.size(); i++) {
            for (uint j = 0U; j < lines.size(); j++) {
                if (i == j) { continue; }

                std::list<std::shared_ptr<Pair>> literalList{};
                auto root = parsePair(lines[i], 0U, lines[i].size(), 0U, literalList);

                std::list<std::shared_ptr<Pair>> horizNew{};
                auto next = parsePair(lines[j], 0U, lines[j].size(), 0U, horizNew);

                root = addPair(root, next, literalList, horizNew);

                bool didEvaluation = true;
                while (didEvaluation) {
                    didEvaluation = doOneEvaluation(root, literalList);
                }

                if (auto magnitude = root->getMagnitude(); magnitude > biggestMagnitude) {
                    biggestMagnitude = magnitude;
                }
            }
        }
        return std::to_string(biggestMagnitude);
    }
};

static_assert(DaySolver<Day18>);

#endif //AOC2021_CPP_DAY18_H