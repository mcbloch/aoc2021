
#ifndef AOC2021_CPP_DAY06_H
#define AOC2021_CPP_DAY06_H


#include <unordered_map>
#include <ranges>
#include <charconv>
#include <numeric>
#include <deque>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/ring_buffer.h"

struct Day06 {
    static const int idx = 6;

    static inline std::string countFishAfterDays(const std::string &inputFilePath, int days) {
        std::ifstream inputFile(inputFilePath);
        std::string line;
        if (inputFile.is_open()) {
            (void) getline(inputFile, line);
            inputFile.close();
        } else {
            return "Could not find or open input file.";
        }

        RingBuffer<long, 9> fishPerAge;

        auto fishs = FileUtils::splitStringInts(line, ",");

        // Seed the fish count list
        for (auto fish : fishs) {
            fishPerAge[fish]++;
        }

        // Loop over the days and age the fish
        for (int day = 1; day <= days; ++day) {
            long fish0 = fishPerAge[0];
            ++fishPerAge;
            fishPerAge[6] += fish0;
        }
        // Count the total amount of fish
        long sum = std::accumulate(fishPerAge.data().begin(), fishPerAge.data().end(), 0L);

        return std::to_string(sum);
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        return countFishAfterDays(inputFilePath, 80);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        return countFishAfterDays(inputFilePath, 256);
    }
};

static_assert(DaySolver<Day06>);


#endif //AOC2021_CPP_DAY06_H