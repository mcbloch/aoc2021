#ifndef AOC2021_CPP_DAY04_H
#define AOC2021_CPP_DAY04_H

#include <iostream>
#include <sstream>
#include <numeric>
#include <vector>
#include <unordered_map>

#include <string>
#include <string_view>
#include <charconv>

#include "utils/runUtils.h"
#include "utils/fileUtils.h"

typedef bool success;

struct Position {
    uint8_t row;
    uint8_t col;
};
struct Field {
    bool marked = false;
    Position pos = {};
};

struct Board {
    std::array<int, 5> rowMarkCount{};
    std::array<int, 5> colMarkCount{};
    bool finished = false;

    std::unordered_map<int, Field> numbers{};

    void addNumber(int number, uint8_t row, uint8_t col) {
        (void) numbers.insert({number, {false, {row, col}}});
    }

    /**
     * @param draw Number that is drawn
     * @return A tuple of a boolean if the board won and the score if it did
     */
    std::tuple<bool, int> addDraw(int draw) {
        if (finished) { return {false, 0}; }
        if (numbers.contains(draw)) {
            numbers[draw].marked = true;
            auto const &pos = numbers[draw].pos;

            rowMarkCount[pos.row] += 1;
            colMarkCount[pos.col] += 1;

            if (rowMarkCount[pos.row] == 5 || colMarkCount[pos.col] == 5) {
                finished = true;
                return {true, getScore(draw)};
            }

            return {false, 0};
        } else {
            return {false, 0};
        }
    }

    /**
     *
     * @param draw The last number draw resulting in a win
     * @return
     */
    int getScore(int draw) {
        int unmarkedSum = 0;
        for (auto[number, val] : numbers) {
            if (!numbers[number].marked) {
                unmarkedSum += number;
            }
        }
        return unmarkedSum * draw;
    }
};

struct Day04 {
    static const int idx = 4;

    /**
     * Ik moet als ik een nummer heb hem markeren
     * Ik moet de ongemarkeerde nummers kunnen vinden om de score te berekenen
     * Ik moet van elk nummer een positie hebben om de winnaar te vinden
     *      Dit is door te kijken of een kolom of rij vol is.
     * @param inputStream
     * @return
     */
    static std::tuple<success, Board> readBoard(std::ifstream &inputStream) {
        Board board{};
        std::string word;
        for (uint8_t i = 0; i < 5; i++) {
            for (uint8_t j = 0; j < 5; j++) {
                inputStream >> word;
                if (word.empty()) { return {false, board}; }
                board.addNumber(std::stoi(word), i, j);
            }
        }
        return {true, board};
    }

    static std::tuple<std::vector<int>, std::vector<Board>> readInput(const std::string &inputFilePath) {
        if (std::ifstream inputStream(inputFilePath); inputStream.is_open()) {
            std::string initLine;

            (void) getline(inputStream, initLine);
            std::string_view line(initLine.c_str(), initLine.size());

            std::vector<int> draws{};
            size_t startIdx = 0;
            size_t sepIdx;
            int draw;

            while ((sepIdx = line.find(',', startIdx)) != std::string::npos) {
                std::string_view sub = line.substr(startIdx, sepIdx);
                (void) std::from_chars(sub.data(), sub.data() + sub.length(), draw);
                draws.push_back(draw);
                startIdx = sepIdx + 1;
            }

            std::vector<Board> boards{};
            while (getline(inputStream, initLine)) {
                auto[found, board] = readBoard(inputStream);
                if (!found) { break; }
                else { boards.push_back(board); }
            }
            inputStream.close();
            return {draws, boards};
        } else {
            throw std::runtime_error("Could not find or open input file.");
        }
    }

    /**
     * Find the first board that wins the bingo game and return it's score
     */
    static inline std::string ex1(const std::string &inputFilePath) {
        auto[draws, boards] = readInput(inputFilePath);

        for (auto draw : draws) {
            for (auto &board : boards) {
                auto[finished, score] = board.addDraw(draw);
                if (finished) {
                    return std::to_string(score);
                }
            }
        }
        return "No answer found";
    }

    /**
     * Find the last board that wins the bingo game and return it's score
     */
    static inline std::string ex2(const std::string &inputFilePath) {
        int lastScore = 0;
        auto[draws, boards] = readInput(inputFilePath);
        for (auto draw : draws) {
            for (auto &board : boards) {
                auto[finished, score] = board.addDraw(draw);
                if (finished) {
                    lastScore = score;
                }
            }
        }

        return std::to_string(lastScore);
    }
};

static_assert(DaySolver<Day04>);

#endif //AOC2021_CPP_DAY04_H
