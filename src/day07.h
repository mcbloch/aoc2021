#ifndef AOC2021_CPP_DAY07_H
#define AOC2021_CPP_DAY07_H

#include <unordered_map>
#include <ranges>
#include <charconv>
#include <numeric>
#include <deque>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/ring_buffer.h"


typedef int (*funcT)(const std::vector<int> &, int);

struct Day07 {
    static const int idx = 7;

    static inline int deviationSum(const std::vector<int> &nums, int center) {
        return std::accumulate(nums.begin(), nums.end(), 0, [&](auto acc, auto num) {
            return acc + abs(num - center);
        });
    }

    /**
     * Cost of a move if the crabs need more energy for further distances
     */
    static inline int moveCost(int moveDistance) {
        return moveDistance * (moveDistance + 1) / 2;
    }

    static inline int exponentialDeviationSum(const std::vector<int> &nums, int center) {
        return std::accumulate(nums.begin(), nums.end(), 0, [&](auto acc, auto num) {
            return acc + moveCost(abs(num - center));
        });
    }

    static inline int minimizeDeviationSum(const std::vector<int> &nums, int startCenterValue, funcT deviationFunc) {
        // Starting center point and the deviation
        int center = startCenterValue;
        int devSum = deviationFunc(nums, center);

        // Minimize the sum of deviations
        int direction = 1;
        while (true) {
            center += direction;
            int newDevSum = deviationFunc(nums, center);
            if (direction == 1 && newDevSum > devSum) {
                center -= 1;
                direction = -1;
            } else if (direction == -1 && newDevSum > devSum) {
                center += 1;
                break;
            } else {
                devSum = newDevSum;
            }
        }
        return devSum;
    }

    /**
     * Efficient way to get the median without needing a sort
     */
    static inline int median(vector<int> &v) {
        size_t n = v.size() / 2;
        nth_element(v.begin(), v.begin() + n, v.end());
        return v[n];
    }

    static inline std::string ex1(const std::string &inputFilePath) {
        std::vector<int> crabPositions = FileUtils::splitStringInts(FileUtils::readFile(inputFilePath), ",");
        return std::to_string(minimizeDeviationSum(crabPositions, median(crabPositions), deviationSum));
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        std::vector<int> crabPositions = FileUtils::splitStringInts(FileUtils::readFile(inputFilePath), ",");
        int avg =
                std::accumulate(crabPositions.begin(), crabPositions.end(), 0) /
                static_cast<int>(crabPositions.size());
        int res = minimizeDeviationSum(crabPositions, avg, exponentialDeviationSum);
        return std::to_string(res);
    }
};

static_assert(DaySolver<Day07>);


#endif //AOC2021_CPP_DAY07_H