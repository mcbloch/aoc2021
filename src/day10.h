#ifndef AOC2021_CPP_DAY10_H
#define AOC2021_CPP_DAY10_H

#include <string>
#include <vector>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Day10 {
    static const int idx = 10;

    static inline std::string ex1(const std::string &inputFilePath) {
        return "";
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        return "";
    }
};

static_assert(DaySolver<Day10>);

#endif //AOC2021_CPP_DAY10_H