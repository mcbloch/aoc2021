#ifndef AOC2021_CPP_DAY11_H
#define AOC2021_CPP_DAY11_H

#include <string>
#include <vector>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/grid.h"

struct Day11 {
    static const int idx = 11;

    static inline std::string ex1(const std::string &inputFilePath) {
        Grid grid = Grid<int>::fromFile<int, int>(inputFilePath, -1, [](int ch) { return ch - '0'; });

        int flashCount = 0;
        for (int step = 0; step < 100; ++step) {
            grid.eachSave([](int v) { return v + 1; });

            bool flashHappened = true;
            while (flashHappened) {
                flashHappened = false;
                grid.each([&](uint r, uint c, int value) {
                    if (value > 9) {
                        flashCount++;
                        flashHappened = true;
                        (void) grid.set(r, c, -1);
                        // Propagate our flash
                        grid.eachNeighbor(r, c, true, [](int v) { return v >= 0 ? v + 1 : v; });
                    }
                });
            }
            grid.eachSave([](int v) { return v == -1 ? 0 : v; });
        }

        return std::to_string(flashCount);
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        Grid grid = Grid<int>::fromFile<int, int>(inputFilePath, -1, [](int ch) { return ch - '0'; });

        int step = 1;
        while (true) {
            grid.eachSave([](int v) { return v + 1; });

            uint flashCount = 0U;

            bool flashHappened = true;
            while (flashHappened) {
                flashHappened = false;
                grid.each([&](int r, int c, int v) {
                    if (grid.get(r, c) > 9) {
                        flashCount++;
                        flashHappened = true;
                        (void) grid.set(r, c, -1);
                        // Propagate our flash
                        grid.eachNeighbor(r, c, true, [](int v) { return v >= 0 ? v + 1 : v; });
                    }
                });
            }
            if (flashCount == grid.size()) {
                break;
            }

            grid.eachSave([](int v) { return v == -1 ? 0 : v; });
            step++;

        }
        return std::to_string(step);
    }
};

static_assert(DaySolver<Day11>);

#endif //AOC2021_CPP_DAY11_H