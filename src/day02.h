#ifndef AOC2021_CPP_DAY02_H
#define AOC2021_CPP_DAY02_H

#include <string>
#include <utility>
#include <vector>

#include "interpreter/interpreter.h"
#include "utils/utils.h"
#include "utils/fileUtils.h"
#include "utils/runUtils.h"

struct Day02 {
    static const int idx = 2;

    static inline std::string ex1(const std::string &inputFilePath) {
        if (std::ifstream inputStream(inputFilePath); inputStream.is_open()) {
            std::string line;

            int horizontal = 0;
            int depth = 0;

            while (getline(inputStream, line)) {
                int amount = std::stoi(line.substr(line.find(' ') + 1, line.size()));

                if (line[0] == 'f') {
                    horizontal += amount;
                } else if (line[0] == 'd') {
                    depth += amount;
                } else if (line[0] == 'u') {
                    depth -= amount;
                } else {
                    return "Unknown direction: " + std::to_string(line[0]);
                }
            }
            inputStream.close();
            return std::to_string(horizontal * depth);
        } else {
            return "Could not find or open input file.";
        }
    }
    static inline std::string ex2(const std::string &inputFile){
        return Interpreter().interpretFile(inputFile);
    }
/*
    static inline std::string ex2(const std::string &inputFilePath) {
        if (std::ifstream inputStream(inputFilePath); inputStream.is_open()) {
            std::string line;

            int horizontal = 0;
            int depth = 0;
            int aim = 0;

            while (getline(inputStream, line)) {
                i
                nt amount = std::stoi(line.substr(line.find(' ') + 1, line.size()));

                if (line[0] == 'f') {
                    horizontal += amount;
                    depth += aim * amount;
                } else if (line[0] == 'd') {
                    aim += amount;
                } else if (line[0] == 'u') {
                    aim -= amount;
                } else {
                    return "Unknown direction: " + std::to_string(line[0]);
                }
            }
            inputStream.close();
            return std::to_string(horizontal * depth);
        } else {
            return "Could not find or open input file.";
        }
    }*/
};

static_assert(DaySolver<Day02>);

#endif //AOC2021_CPP_DAY02_H
