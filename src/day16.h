#ifndef AOC2021_CPP_DAY16_H
#define AOC2021_CPP_DAY16_H

#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <bitset>

#include "utils/fileUtils.h"
#include "utils/runUtils.h"
#include "utils/strUtils.h"

#include "hexops/parser.h"
#include "hexops/reader.h"
#include "hexops/types.h"

struct Day16 {
    static const int idx = 16;

    static inline std::string ex1(const std::string &inputFilePath) {
        std::string strBin = reader(inputFilePath);
        auto[p, _] = parsePacket(strBin);
        return std::to_string(p->countVersionSums());
    }

    static inline std::string ex2(const std::string &inputFilePath) {
        std::string strBin = reader(inputFilePath);
        auto[p, _] = parsePacket(strBin);
        return std::to_string(p->evaluate());
    }
};

static_assert(DaySolver<Day16>);

#endif //AOC2021_CPP_DAY16_H