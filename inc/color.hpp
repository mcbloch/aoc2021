#ifndef AOC2021_CPP_COLOR_HPP
#define AOC2021_CPP_COLOR_HPP

// https://misc.flogisoft.com/bash/tip_colors_and_formatting

//the following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */

#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

#define BRIGHTBLACK  "\033[30;1m"
#define BRIGHTRED  "\033[31;1m"
#define BRIGHTGREEN  "\033[32;1m"
#define BRIGHTYELLOW  "\033[33;1m"
#define BRIGHTBLUE "\033[34;1m"
#define BRIGHTMAGENTA "\033[35;1m"
#define BRIGHTCYAN "\033[36;1m"
#define BRIGHTWHITE "\033[37;1m"

#define BOLD "\033[1m"
#define DIM "\033[2m"
#define UNDERLINE "\033[4m"
#define BLINK "\033[5m"
#define REVERSED "\033[7m"

#define BLINKCYAN "\033[36;5m"

#endif